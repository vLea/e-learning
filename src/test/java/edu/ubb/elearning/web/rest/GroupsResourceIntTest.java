package edu.ubb.elearning.web.rest;

import edu.ubb.elearning.Application;
import edu.ubb.elearning.domain.Groups;
import edu.ubb.elearning.repository.GroupsRepository;
import edu.ubb.elearning.service.GroupsService;
import edu.ubb.elearning.web.rest.dto.GroupsDTO;
import edu.ubb.elearning.web.rest.mapper.GroupsMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the GroupsResource REST controller.
 *
 * @see GroupsResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class GroupsResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAA";
    private static final String UPDATED_NAME = "BBBBB";

    @Inject
    private GroupsRepository groupsRepository;

    @Inject
    private GroupsService groupsService;
    
    @Inject
    private GroupsMapper groupsMapper;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restGroupsMockMvc;

    private Groups groups;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        GroupsResource groupsResource = new GroupsResource();
        ReflectionTestUtils.setField(groupsResource, "groupsService", groupsService);
        ReflectionTestUtils.setField(groupsResource, "groupsMapper", groupsMapper);
        this.restGroupsMockMvc = MockMvcBuilders.standaloneSetup(groupsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        groups = new Groups();
        groups.setName(DEFAULT_NAME);
    }

    @Test
    @Transactional
    public void createGroups() throws Exception {
        int databaseSizeBeforeCreate = groupsRepository.findAll().size();

        // Create the Groups
        GroupsDTO groupsDTO = groupsMapper.groupsToGroupsDTO(groups);

        restGroupsMockMvc.perform(post("/api/groupss")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(groupsDTO)))
                .andExpect(status().isCreated());

        // Validate the Groups in the database
        List<Groups> groupss = groupsRepository.findAll();
        assertThat(groupss).hasSize(databaseSizeBeforeCreate + 1);
        Groups testGroups = groupss.get(groupss.size() - 1);
        assertThat(testGroups.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    public void getAllGroupss() throws Exception {
        // Initialize the database
        groupsRepository.saveAndFlush(groups);

        // Get all the groupss
        restGroupsMockMvc.perform(get("/api/groupss"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(groups.getId().intValue())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }

    @Test
    @Transactional
    public void getGroups() throws Exception {
        // Initialize the database
        groupsRepository.saveAndFlush(groups);

        // Get the groups
        restGroupsMockMvc.perform(get("/api/groupss/{id}", groups.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(groups.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingGroups() throws Exception {
        // Get the groups
        restGroupsMockMvc.perform(get("/api/groupss/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateGroups() throws Exception {
        // Initialize the database
        groupsRepository.saveAndFlush(groups);

		int databaseSizeBeforeUpdate = groupsRepository.findAll().size();

        // Update the groups
        groups.setName(UPDATED_NAME);
        GroupsDTO groupsDTO = groupsMapper.groupsToGroupsDTO(groups);

        restGroupsMockMvc.perform(put("/api/groupss")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(groupsDTO)))
                .andExpect(status().isOk());

        // Validate the Groups in the database
        List<Groups> groupss = groupsRepository.findAll();
        assertThat(groupss).hasSize(databaseSizeBeforeUpdate);
        Groups testGroups = groupss.get(groupss.size() - 1);
        assertThat(testGroups.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    public void deleteGroups() throws Exception {
        // Initialize the database
        groupsRepository.saveAndFlush(groups);

		int databaseSizeBeforeDelete = groupsRepository.findAll().size();

        // Get the groups
        restGroupsMockMvc.perform(delete("/api/groupss/{id}", groups.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Groups> groupss = groupsRepository.findAll();
        assertThat(groupss).hasSize(databaseSizeBeforeDelete - 1);
    }
}
