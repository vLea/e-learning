package edu.ubb.elearning.web.rest;

import edu.ubb.elearning.Application;
import edu.ubb.elearning.domain.Assignment;
import edu.ubb.elearning.repository.AssignmentRepository;
import edu.ubb.elearning.service.AssignmentService;
import edu.ubb.elearning.web.rest.dto.AssignmentDTO;
import edu.ubb.elearning.web.rest.mapper.AssignmentMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the AssignmentResource REST controller.
 *
 * @see AssignmentResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class AssignmentResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAA";
    private static final String UPDATED_NAME = "BBBBB";
    private static final String DEFAULT_DESCRIPTION = "AAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBB";

    private static final LocalDate DEFAULT_DEADLINE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DEADLINE = LocalDate.now(ZoneId.systemDefault());

    private static final Double DEFAULT_POINTS = 1D;
    private static final Double UPDATED_POINTS = 2D;

    @Inject
    private AssignmentRepository assignmentRepository;

    @Inject
    private AssignmentService assignmentService;
    
    @Inject
    private AssignmentMapper assignmentMapper;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restAssignmentMockMvc;

    private Assignment assignment;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        AssignmentResource assignmentResource = new AssignmentResource();
        ReflectionTestUtils.setField(assignmentResource, "assignmentService", assignmentService);
        ReflectionTestUtils.setField(assignmentResource, "assignmentMapper", assignmentMapper);
        this.restAssignmentMockMvc = MockMvcBuilders.standaloneSetup(assignmentResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        assignment = new Assignment();
        assignment.setName(DEFAULT_NAME);
        assignment.setDescription(DEFAULT_DESCRIPTION);
        assignment.setDeadline(DEFAULT_DEADLINE);
        assignment.setPoints(DEFAULT_POINTS);
    }

    @Test
    @Transactional
    public void createAssignment() throws Exception {
        int databaseSizeBeforeCreate = assignmentRepository.findAll().size();

        // Create the Assignment
        AssignmentDTO assignmentDTO = assignmentMapper.assignmentToAssignmentDTO(assignment);

        restAssignmentMockMvc.perform(post("/api/assignments")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(assignmentDTO)))
                .andExpect(status().isCreated());

        // Validate the Assignment in the database
        List<Assignment> assignments = assignmentRepository.findAll();
        assertThat(assignments).hasSize(databaseSizeBeforeCreate + 1);
        Assignment testAssignment = assignments.get(assignments.size() - 1);
        assertThat(testAssignment.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testAssignment.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testAssignment.getDeadline()).isEqualTo(DEFAULT_DEADLINE);
        assertThat(testAssignment.getPoints()).isEqualTo(DEFAULT_POINTS);
    }

    @Test
    @Transactional
    public void getAllAssignments() throws Exception {
        // Initialize the database
        assignmentRepository.saveAndFlush(assignment);

        // Get all the assignments
        restAssignmentMockMvc.perform(get("/api/assignments"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(assignment.getId().intValue())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
                .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
                .andExpect(jsonPath("$.[*].deadline").value(hasItem(DEFAULT_DEADLINE.toString())))
                .andExpect(jsonPath("$.[*].points").value(hasItem(DEFAULT_POINTS.doubleValue())));
    }

    @Test
    @Transactional
    public void getAssignment() throws Exception {
        // Initialize the database
        assignmentRepository.saveAndFlush(assignment);

        // Get the assignment
        restAssignmentMockMvc.perform(get("/api/assignments/{id}", assignment.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(assignment.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.deadline").value(DEFAULT_DEADLINE.toString()))
            .andExpect(jsonPath("$.points").value(DEFAULT_POINTS.doubleValue()));
    }

    @Test
    @Transactional
    public void getNonExistingAssignment() throws Exception {
        // Get the assignment
        restAssignmentMockMvc.perform(get("/api/assignments/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAssignment() throws Exception {
        // Initialize the database
        assignmentRepository.saveAndFlush(assignment);

		int databaseSizeBeforeUpdate = assignmentRepository.findAll().size();

        // Update the assignment
        assignment.setName(UPDATED_NAME);
        assignment.setDescription(UPDATED_DESCRIPTION);
        assignment.setDeadline(UPDATED_DEADLINE);
        assignment.setPoints(UPDATED_POINTS);
        AssignmentDTO assignmentDTO = assignmentMapper.assignmentToAssignmentDTO(assignment);

        restAssignmentMockMvc.perform(put("/api/assignments")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(assignmentDTO)))
                .andExpect(status().isOk());

        // Validate the Assignment in the database
        List<Assignment> assignments = assignmentRepository.findAll();
        assertThat(assignments).hasSize(databaseSizeBeforeUpdate);
        Assignment testAssignment = assignments.get(assignments.size() - 1);
        assertThat(testAssignment.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testAssignment.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testAssignment.getDeadline()).isEqualTo(UPDATED_DEADLINE);
        assertThat(testAssignment.getPoints()).isEqualTo(UPDATED_POINTS);
    }

    @Test
    @Transactional
    public void deleteAssignment() throws Exception {
        // Initialize the database
        assignmentRepository.saveAndFlush(assignment);

		int databaseSizeBeforeDelete = assignmentRepository.findAll().size();

        // Get the assignment
        restAssignmentMockMvc.perform(delete("/api/assignments/{id}", assignment.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Assignment> assignments = assignmentRepository.findAll();
        assertThat(assignments).hasSize(databaseSizeBeforeDelete - 1);
    }
}
