package edu.ubb.elearning.web.rest;

import edu.ubb.elearning.Application;
import edu.ubb.elearning.domain.ConversationComment;
import edu.ubb.elearning.repository.ConversationCommentRepository;
import edu.ubb.elearning.service.ConversationCommentService;
import edu.ubb.elearning.web.rest.dto.ConversationCommentDTO;
import edu.ubb.elearning.web.rest.mapper.ConversationCommentMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the ConversationCommentResource REST controller.
 *
 * @see ConversationCommentResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class ConversationCommentResourceIntTest {

    private static final String DEFAULT_TITLE = "AAAAA";
    private static final String UPDATED_TITLE = "BBBBB";
    private static final String DEFAULT_TEXT = "AAAAA";
    private static final String UPDATED_TEXT = "BBBBB";

    @Inject
    private ConversationCommentRepository conversationCommentRepository;

    @Inject
    private ConversationCommentService conversationCommentService;
    
    @Inject
    private ConversationCommentMapper conversationCommentMapper;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restConversationCommentMockMvc;

    private ConversationComment conversationComment;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ConversationCommentResource conversationCommentResource = new ConversationCommentResource();
        ReflectionTestUtils.setField(conversationCommentResource, "conversationCommentService", conversationCommentService);
        ReflectionTestUtils.setField(conversationCommentResource, "conversationCommentMapper", conversationCommentMapper);
        this.restConversationCommentMockMvc = MockMvcBuilders.standaloneSetup(conversationCommentResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        conversationComment = new ConversationComment();
        conversationComment.setTitle(DEFAULT_TITLE);
        conversationComment.setText(DEFAULT_TEXT);
    }

    @Test
    @Transactional
    public void createConversationComment() throws Exception {
        int databaseSizeBeforeCreate = conversationCommentRepository.findAll().size();

        // Create the ConversationComment
        ConversationCommentDTO conversationCommentDTO = conversationCommentMapper.conversationCommentToConversationCommentDTO(conversationComment);

        restConversationCommentMockMvc.perform(post("/api/conversationComments")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(conversationCommentDTO)))
                .andExpect(status().isCreated());

        // Validate the ConversationComment in the database
        List<ConversationComment> conversationComments = conversationCommentRepository.findAll();
        assertThat(conversationComments).hasSize(databaseSizeBeforeCreate + 1);
        ConversationComment testConversationComment = conversationComments.get(conversationComments.size() - 1);
        assertThat(testConversationComment.getTitle()).isEqualTo(DEFAULT_TITLE);
        assertThat(testConversationComment.getText()).isEqualTo(DEFAULT_TEXT);
    }

    @Test
    @Transactional
    public void getAllConversationComments() throws Exception {
        // Initialize the database
        conversationCommentRepository.saveAndFlush(conversationComment);

        // Get all the conversationComments
        restConversationCommentMockMvc.perform(get("/api/conversationComments"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(conversationComment.getId().intValue())))
                .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE.toString())))
                .andExpect(jsonPath("$.[*].text").value(hasItem(DEFAULT_TEXT.toString())));
    }

    @Test
    @Transactional
    public void getConversationComment() throws Exception {
        // Initialize the database
        conversationCommentRepository.saveAndFlush(conversationComment);

        // Get the conversationComment
        restConversationCommentMockMvc.perform(get("/api/conversationComments/{id}", conversationComment.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(conversationComment.getId().intValue()))
            .andExpect(jsonPath("$.title").value(DEFAULT_TITLE.toString()))
            .andExpect(jsonPath("$.text").value(DEFAULT_TEXT.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingConversationComment() throws Exception {
        // Get the conversationComment
        restConversationCommentMockMvc.perform(get("/api/conversationComments/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateConversationComment() throws Exception {
        // Initialize the database
        conversationCommentRepository.saveAndFlush(conversationComment);

		int databaseSizeBeforeUpdate = conversationCommentRepository.findAll().size();

        // Update the conversationComment
        conversationComment.setTitle(UPDATED_TITLE);
        conversationComment.setText(UPDATED_TEXT);
        ConversationCommentDTO conversationCommentDTO = conversationCommentMapper.conversationCommentToConversationCommentDTO(conversationComment);

        restConversationCommentMockMvc.perform(put("/api/conversationComments")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(conversationCommentDTO)))
                .andExpect(status().isOk());

        // Validate the ConversationComment in the database
        List<ConversationComment> conversationComments = conversationCommentRepository.findAll();
        assertThat(conversationComments).hasSize(databaseSizeBeforeUpdate);
        ConversationComment testConversationComment = conversationComments.get(conversationComments.size() - 1);
        assertThat(testConversationComment.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testConversationComment.getText()).isEqualTo(UPDATED_TEXT);
    }

    @Test
    @Transactional
    public void deleteConversationComment() throws Exception {
        // Initialize the database
        conversationCommentRepository.saveAndFlush(conversationComment);

		int databaseSizeBeforeDelete = conversationCommentRepository.findAll().size();

        // Get the conversationComment
        restConversationCommentMockMvc.perform(delete("/api/conversationComments/{id}", conversationComment.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<ConversationComment> conversationComments = conversationCommentRepository.findAll();
        assertThat(conversationComments).hasSize(databaseSizeBeforeDelete - 1);
    }
}
