package edu.ubb.elearning.web.rest;

import edu.ubb.elearning.Application;
import edu.ubb.elearning.domain.Tests;
import edu.ubb.elearning.repository.TestsRepository;
import edu.ubb.elearning.service.TestsService;
import edu.ubb.elearning.web.rest.dto.TestsDTO;
import edu.ubb.elearning.web.rest.mapper.TestsMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import edu.ubb.elearning.domain.enumeration.TestType;

/**
 * Test class for the TestsResource REST controller.
 *
 * @see TestsResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class TestsResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAA";
    private static final String UPDATED_NAME = "BBBBB";
    private static final String DEFAULT_DESCRIPTION = "AAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBB";

    private static final LocalDate DEFAULT_DEADLINE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DEADLINE = LocalDate.now(ZoneId.systemDefault());


private static final TestType DEFAULT_TYPE = TestType.TYPE1;
    private static final TestType UPDATED_TYPE = TestType.TYPE2;

    private static final LocalDate DEFAULT_STARTING_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_STARTING_DATE = LocalDate.now(ZoneId.systemDefault());

    @Inject
    private TestsRepository testsRepository;

    @Inject
    private TestsService testsService;
    
    @Inject
    private TestsMapper testsMapper;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restTestsMockMvc;

    private Tests tests;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        TestsResource testsResource = new TestsResource();
        ReflectionTestUtils.setField(testsResource, "testsService", testsService);
        ReflectionTestUtils.setField(testsResource, "testsMapper", testsMapper);
        this.restTestsMockMvc = MockMvcBuilders.standaloneSetup(testsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        tests = new Tests();
        tests.setName(DEFAULT_NAME);
        tests.setDescription(DEFAULT_DESCRIPTION);
        tests.setDeadline(DEFAULT_DEADLINE);
        tests.setType(DEFAULT_TYPE);
        tests.setStartingDate(DEFAULT_STARTING_DATE);
    }

    @Test
    @Transactional
    public void createTests() throws Exception {
        int databaseSizeBeforeCreate = testsRepository.findAll().size();

        // Create the Tests
        TestsDTO testsDTO = testsMapper.testsToTestsDTO(tests);

        restTestsMockMvc.perform(post("/api/testss")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(testsDTO)))
                .andExpect(status().isCreated());

        // Validate the Tests in the database
        List<Tests> testss = testsRepository.findAll();
        assertThat(testss).hasSize(databaseSizeBeforeCreate + 1);
        Tests testTests = testss.get(testss.size() - 1);
        assertThat(testTests.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testTests.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testTests.getDeadline()).isEqualTo(DEFAULT_DEADLINE);
        assertThat(testTests.getType()).isEqualTo(DEFAULT_TYPE);
        assertThat(testTests.getStartingDate()).isEqualTo(DEFAULT_STARTING_DATE);
    }

    @Test
    @Transactional
    public void getAllTestss() throws Exception {
        // Initialize the database
        testsRepository.saveAndFlush(tests);

        // Get all the testss
        restTestsMockMvc.perform(get("/api/testss"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(tests.getId().intValue())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
                .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
                .andExpect(jsonPath("$.[*].deadline").value(hasItem(DEFAULT_DEADLINE.toString())))
                .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())))
                .andExpect(jsonPath("$.[*].startingDate").value(hasItem(DEFAULT_STARTING_DATE.toString())));
    }

    @Test
    @Transactional
    public void getTests() throws Exception {
        // Initialize the database
        testsRepository.saveAndFlush(tests);

        // Get the tests
        restTestsMockMvc.perform(get("/api/testss/{id}", tests.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(tests.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.deadline").value(DEFAULT_DEADLINE.toString()))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE.toString()))
            .andExpect(jsonPath("$.startingDate").value(DEFAULT_STARTING_DATE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingTests() throws Exception {
        // Get the tests
        restTestsMockMvc.perform(get("/api/testss/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTests() throws Exception {
        // Initialize the database
        testsRepository.saveAndFlush(tests);

		int databaseSizeBeforeUpdate = testsRepository.findAll().size();

        // Update the tests
        tests.setName(UPDATED_NAME);
        tests.setDescription(UPDATED_DESCRIPTION);
        tests.setDeadline(UPDATED_DEADLINE);
        tests.setType(UPDATED_TYPE);
        tests.setStartingDate(UPDATED_STARTING_DATE);
        TestsDTO testsDTO = testsMapper.testsToTestsDTO(tests);

        restTestsMockMvc.perform(put("/api/testss")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(testsDTO)))
                .andExpect(status().isOk());

        // Validate the Tests in the database
        List<Tests> testss = testsRepository.findAll();
        assertThat(testss).hasSize(databaseSizeBeforeUpdate);
        Tests testTests = testss.get(testss.size() - 1);
        assertThat(testTests.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testTests.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testTests.getDeadline()).isEqualTo(UPDATED_DEADLINE);
        assertThat(testTests.getType()).isEqualTo(UPDATED_TYPE);
        assertThat(testTests.getStartingDate()).isEqualTo(UPDATED_STARTING_DATE);
    }

    @Test
    @Transactional
    public void deleteTests() throws Exception {
        // Initialize the database
        testsRepository.saveAndFlush(tests);

		int databaseSizeBeforeDelete = testsRepository.findAll().size();

        // Get the tests
        restTestsMockMvc.perform(delete("/api/testss/{id}", tests.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Tests> testss = testsRepository.findAll();
        assertThat(testss).hasSize(databaseSizeBeforeDelete - 1);
    }
}
