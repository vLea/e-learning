package edu.ubb.elearning.web.rest;

import edu.ubb.elearning.Application;
import edu.ubb.elearning.domain.Pages;
import edu.ubb.elearning.repository.PagesRepository;
import edu.ubb.elearning.service.PagesService;
import edu.ubb.elearning.web.rest.dto.PagesDTO;
import edu.ubb.elearning.web.rest.mapper.PagesMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the PagesResource REST controller.
 *
 * @see PagesResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class PagesResourceIntTest {


    @Inject
    private PagesRepository pagesRepository;

    @Inject
    private PagesService pagesService;
    
    @Inject
    private PagesMapper pagesMapper;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restPagesMockMvc;

    private Pages pages;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        PagesResource pagesResource = new PagesResource();
        ReflectionTestUtils.setField(pagesResource, "pagesService", pagesService);
        ReflectionTestUtils.setField(pagesResource, "pagesMapper", pagesMapper);
        this.restPagesMockMvc = MockMvcBuilders.standaloneSetup(pagesResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        pages = new Pages();
    }

    @Test
    @Transactional
    public void createPages() throws Exception {
        int databaseSizeBeforeCreate = pagesRepository.findAll().size();

        // Create the Pages
        PagesDTO pagesDTO = pagesMapper.pagesToPagesDTO(pages);

        restPagesMockMvc.perform(post("/api/pagess")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(pagesDTO)))
                .andExpect(status().isCreated());

        // Validate the Pages in the database
        List<Pages> pagess = pagesRepository.findAll();
        assertThat(pagess).hasSize(databaseSizeBeforeCreate + 1);
        Pages testPages = pagess.get(pagess.size() - 1);
    }

    @Test
    @Transactional
    public void getAllPagess() throws Exception {
        // Initialize the database
        pagesRepository.saveAndFlush(pages);

        // Get all the pagess
        restPagesMockMvc.perform(get("/api/pagess"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(pages.getId().intValue())));
    }

    @Test
    @Transactional
    public void getPages() throws Exception {
        // Initialize the database
        pagesRepository.saveAndFlush(pages);

        // Get the pages
        restPagesMockMvc.perform(get("/api/pagess/{id}", pages.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(pages.getId().intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingPages() throws Exception {
        // Get the pages
        restPagesMockMvc.perform(get("/api/pagess/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePages() throws Exception {
        // Initialize the database
        pagesRepository.saveAndFlush(pages);

		int databaseSizeBeforeUpdate = pagesRepository.findAll().size();

        // Update the pages
        PagesDTO pagesDTO = pagesMapper.pagesToPagesDTO(pages);

        restPagesMockMvc.perform(put("/api/pagess")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(pagesDTO)))
                .andExpect(status().isOk());

        // Validate the Pages in the database
        List<Pages> pagess = pagesRepository.findAll();
        assertThat(pagess).hasSize(databaseSizeBeforeUpdate);
        Pages testPages = pagess.get(pagess.size() - 1);
    }

    @Test
    @Transactional
    public void deletePages() throws Exception {
        // Initialize the database
        pagesRepository.saveAndFlush(pages);

		int databaseSizeBeforeDelete = pagesRepository.findAll().size();

        // Get the pages
        restPagesMockMvc.perform(delete("/api/pagess/{id}", pages.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Pages> pagess = pagesRepository.findAll();
        assertThat(pagess).hasSize(databaseSizeBeforeDelete - 1);
    }
}
