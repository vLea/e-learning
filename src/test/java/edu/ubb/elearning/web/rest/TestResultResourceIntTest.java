package edu.ubb.elearning.web.rest;

import edu.ubb.elearning.Application;
import edu.ubb.elearning.domain.TestResult;
import edu.ubb.elearning.repository.TestResultRepository;
import edu.ubb.elearning.service.TestResultService;
import edu.ubb.elearning.web.rest.dto.TestResultDTO;
import edu.ubb.elearning.web.rest.mapper.TestResultMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the TestResultResource REST controller.
 *
 * @see TestResultResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class TestResultResourceIntTest {


    private static final Double DEFAULT_MARK = 1D;
    private static final Double UPDATED_MARK = 2D;

    @Inject
    private TestResultRepository testResultRepository;

    @Inject
    private TestResultService testResultService;
    
    @Inject
    private TestResultMapper testResultMapper;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restTestResultMockMvc;

    private TestResult testResult;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        TestResultResource testResultResource = new TestResultResource();
        ReflectionTestUtils.setField(testResultResource, "testResultService", testResultService);
        ReflectionTestUtils.setField(testResultResource, "testResultMapper", testResultMapper);
        this.restTestResultMockMvc = MockMvcBuilders.standaloneSetup(testResultResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        testResult = new TestResult();
        testResult.setMark(DEFAULT_MARK);
    }

    @Test
    @Transactional
    public void createTestResult() throws Exception {
        int databaseSizeBeforeCreate = testResultRepository.findAll().size();

        // Create the TestResult
        TestResultDTO testResultDTO = testResultMapper.testResultToTestResultDTO(testResult);

        restTestResultMockMvc.perform(post("/api/testResults")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(testResultDTO)))
                .andExpect(status().isCreated());

        // Validate the TestResult in the database
        List<TestResult> testResults = testResultRepository.findAll();
        assertThat(testResults).hasSize(databaseSizeBeforeCreate + 1);
        TestResult testTestResult = testResults.get(testResults.size() - 1);
        assertThat(testTestResult.getMark()).isEqualTo(DEFAULT_MARK);
    }

    @Test
    @Transactional
    public void getAllTestResults() throws Exception {
        // Initialize the database
        testResultRepository.saveAndFlush(testResult);

        // Get all the testResults
        restTestResultMockMvc.perform(get("/api/testResults"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(testResult.getId().intValue())))
                .andExpect(jsonPath("$.[*].mark").value(hasItem(DEFAULT_MARK.doubleValue())));
    }

    @Test
    @Transactional
    public void getTestResult() throws Exception {
        // Initialize the database
        testResultRepository.saveAndFlush(testResult);

        // Get the testResult
        restTestResultMockMvc.perform(get("/api/testResults/{id}", testResult.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(testResult.getId().intValue()))
            .andExpect(jsonPath("$.mark").value(DEFAULT_MARK.doubleValue()));
    }

    @Test
    @Transactional
    public void getNonExistingTestResult() throws Exception {
        // Get the testResult
        restTestResultMockMvc.perform(get("/api/testResults/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTestResult() throws Exception {
        // Initialize the database
        testResultRepository.saveAndFlush(testResult);

		int databaseSizeBeforeUpdate = testResultRepository.findAll().size();

        // Update the testResult
        testResult.setMark(UPDATED_MARK);
        TestResultDTO testResultDTO = testResultMapper.testResultToTestResultDTO(testResult);

        restTestResultMockMvc.perform(put("/api/testResults")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(testResultDTO)))
                .andExpect(status().isOk());

        // Validate the TestResult in the database
        List<TestResult> testResults = testResultRepository.findAll();
        assertThat(testResults).hasSize(databaseSizeBeforeUpdate);
        TestResult testTestResult = testResults.get(testResults.size() - 1);
        assertThat(testTestResult.getMark()).isEqualTo(UPDATED_MARK);
    }

    @Test
    @Transactional
    public void deleteTestResult() throws Exception {
        // Initialize the database
        testResultRepository.saveAndFlush(testResult);

		int databaseSizeBeforeDelete = testResultRepository.findAll().size();

        // Get the testResult
        restTestResultMockMvc.perform(delete("/api/testResults/{id}", testResult.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<TestResult> testResults = testResultRepository.findAll();
        assertThat(testResults).hasSize(databaseSizeBeforeDelete - 1);
    }
}
