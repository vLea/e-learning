package edu.ubb.elearning.web.rest;

import edu.ubb.elearning.Application;
import edu.ubb.elearning.domain.ExternalResource;
import edu.ubb.elearning.repository.ExternalResourceRepository;
import edu.ubb.elearning.service.ExternalResourceService;
import edu.ubb.elearning.web.rest.dto.ExternalResourceDTO;
import edu.ubb.elearning.web.rest.mapper.ExternalResourceMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the ExternalResourceResource REST controller.
 *
 * @see ExternalResourceResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class ExternalResourceResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAA";
    private static final String UPDATED_NAME = "BBBBB";
    private static final String DEFAULT_TYPE = "AAAAA";
    private static final String UPDATED_TYPE = "BBBBB";

    private static final byte[] DEFAULT_DATA = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_DATA = TestUtil.createByteArray(2, "1");
    private static final String DEFAULT_DATA_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_DATA_CONTENT_TYPE = "image/png";
    private static final String DEFAULT_PATH = "AAAAA";
    private static final String UPDATED_PATH = "BBBBB";

    @Inject
    private ExternalResourceRepository externalResourceRepository;

    @Inject
    private ExternalResourceService externalResourceService;
    
    @Inject
    private ExternalResourceMapper externalResourceMapper;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restExternalResourceMockMvc;

    private ExternalResource externalResource;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ExternalResourceResource externalResourceResource = new ExternalResourceResource();
        ReflectionTestUtils.setField(externalResourceResource, "externalResourceService", externalResourceService);
        ReflectionTestUtils.setField(externalResourceResource, "externalResourceMapper", externalResourceMapper);
        this.restExternalResourceMockMvc = MockMvcBuilders.standaloneSetup(externalResourceResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        externalResource = new ExternalResource();
        externalResource.setName(DEFAULT_NAME);
        externalResource.setType(DEFAULT_TYPE);
        externalResource.setData(DEFAULT_DATA);
        externalResource.setDataContentType(DEFAULT_DATA_CONTENT_TYPE);
        externalResource.setPath(DEFAULT_PATH);
    }

    @Test
    @Transactional
    public void createExternalResource() throws Exception {
        int databaseSizeBeforeCreate = externalResourceRepository.findAll().size();

        // Create the ExternalResource
        ExternalResourceDTO externalResourceDTO = externalResourceMapper.externalResourceToExternalResourceDTO(externalResource);

        restExternalResourceMockMvc.perform(post("/api/externalResources")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(externalResourceDTO)))
                .andExpect(status().isCreated());

        // Validate the ExternalResource in the database
        List<ExternalResource> externalResources = externalResourceRepository.findAll();
        assertThat(externalResources).hasSize(databaseSizeBeforeCreate + 1);
        ExternalResource testExternalResource = externalResources.get(externalResources.size() - 1);
        assertThat(testExternalResource.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testExternalResource.getType()).isEqualTo(DEFAULT_TYPE);
        assertThat(testExternalResource.getData()).isEqualTo(DEFAULT_DATA);
        assertThat(testExternalResource.getDataContentType()).isEqualTo(DEFAULT_DATA_CONTENT_TYPE);
        assertThat(testExternalResource.getPath()).isEqualTo(DEFAULT_PATH);
    }

    @Test
    @Transactional
    public void getAllExternalResources() throws Exception {
        // Initialize the database
        externalResourceRepository.saveAndFlush(externalResource);

        // Get all the externalResources
        restExternalResourceMockMvc.perform(get("/api/externalResources"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(externalResource.getId().intValue())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
                .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())))
                .andExpect(jsonPath("$.[*].dataContentType").value(hasItem(DEFAULT_DATA_CONTENT_TYPE)))
                .andExpect(jsonPath("$.[*].data").value(hasItem(Base64Utils.encodeToString(DEFAULT_DATA))))
                .andExpect(jsonPath("$.[*].path").value(hasItem(DEFAULT_PATH.toString())));
    }

    @Test
    @Transactional
    public void getExternalResource() throws Exception {
        // Initialize the database
        externalResourceRepository.saveAndFlush(externalResource);

        // Get the externalResource
        restExternalResourceMockMvc.perform(get("/api/externalResources/{id}", externalResource.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(externalResource.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE.toString()))
            .andExpect(jsonPath("$.dataContentType").value(DEFAULT_DATA_CONTENT_TYPE))
            .andExpect(jsonPath("$.data").value(Base64Utils.encodeToString(DEFAULT_DATA)))
            .andExpect(jsonPath("$.path").value(DEFAULT_PATH.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingExternalResource() throws Exception {
        // Get the externalResource
        restExternalResourceMockMvc.perform(get("/api/externalResources/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateExternalResource() throws Exception {
        // Initialize the database
        externalResourceRepository.saveAndFlush(externalResource);

		int databaseSizeBeforeUpdate = externalResourceRepository.findAll().size();

        // Update the externalResource
        externalResource.setName(UPDATED_NAME);
        externalResource.setType(UPDATED_TYPE);
        externalResource.setData(UPDATED_DATA);
        externalResource.setDataContentType(UPDATED_DATA_CONTENT_TYPE);
        externalResource.setPath(UPDATED_PATH);
        ExternalResourceDTO externalResourceDTO = externalResourceMapper.externalResourceToExternalResourceDTO(externalResource);

        restExternalResourceMockMvc.perform(put("/api/externalResources")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(externalResourceDTO)))
                .andExpect(status().isOk());

        // Validate the ExternalResource in the database
        List<ExternalResource> externalResources = externalResourceRepository.findAll();
        assertThat(externalResources).hasSize(databaseSizeBeforeUpdate);
        ExternalResource testExternalResource = externalResources.get(externalResources.size() - 1);
        assertThat(testExternalResource.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testExternalResource.getType()).isEqualTo(UPDATED_TYPE);
        assertThat(testExternalResource.getData()).isEqualTo(UPDATED_DATA);
        assertThat(testExternalResource.getDataContentType()).isEqualTo(UPDATED_DATA_CONTENT_TYPE);
        assertThat(testExternalResource.getPath()).isEqualTo(UPDATED_PATH);
    }

    @Test
    @Transactional
    public void deleteExternalResource() throws Exception {
        // Initialize the database
        externalResourceRepository.saveAndFlush(externalResource);

		int databaseSizeBeforeDelete = externalResourceRepository.findAll().size();

        // Get the externalResource
        restExternalResourceMockMvc.perform(delete("/api/externalResources/{id}", externalResource.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<ExternalResource> externalResources = externalResourceRepository.findAll();
        assertThat(externalResources).hasSize(databaseSizeBeforeDelete - 1);
    }
}
