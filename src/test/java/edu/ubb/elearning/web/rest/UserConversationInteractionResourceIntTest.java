package edu.ubb.elearning.web.rest;

import edu.ubb.elearning.Application;
import edu.ubb.elearning.domain.UserConversationInteraction;
import edu.ubb.elearning.repository.UserConversationInteractionRepository;
import edu.ubb.elearning.service.UserConversationInteractionService;
import edu.ubb.elearning.web.rest.dto.UserConversationInteractionDTO;
import edu.ubb.elearning.web.rest.mapper.UserConversationInteractionMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the UserConversationInteractionResource REST controller.
 *
 * @see UserConversationInteractionResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class UserConversationInteractionResourceIntTest {


    private static final Boolean DEFAULT_ARCHIVED = false;
    private static final Boolean UPDATED_ARCHIVED = true;

    private static final Boolean DEFAULT_FAVORITED = false;
    private static final Boolean UPDATED_FAVORITED = true;

    private static final Boolean DEFAULT_IS_READ = false;
    private static final Boolean UPDATED_IS_READ = true;

    @Inject
    private UserConversationInteractionRepository userConversationInteractionRepository;

    @Inject
    private UserConversationInteractionService userConversationInteractionService;
    
    @Inject
    private UserConversationInteractionMapper userConversationInteractionMapper;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restUserConversationInteractionMockMvc;

    private UserConversationInteraction userConversationInteraction;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        UserConversationInteractionResource userConversationInteractionResource = new UserConversationInteractionResource();
        ReflectionTestUtils.setField(userConversationInteractionResource, "userConversationInteractionService", userConversationInteractionService);
        ReflectionTestUtils.setField(userConversationInteractionResource, "userConversationInteractionMapper", userConversationInteractionMapper);
        this.restUserConversationInteractionMockMvc = MockMvcBuilders.standaloneSetup(userConversationInteractionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        userConversationInteraction = new UserConversationInteraction();
        userConversationInteraction.setArchived(DEFAULT_ARCHIVED);
        userConversationInteraction.setFavorited(DEFAULT_FAVORITED);
        userConversationInteraction.setIsRead(DEFAULT_IS_READ);
    }

    @Test
    @Transactional
    public void createUserConversationInteraction() throws Exception {
        int databaseSizeBeforeCreate = userConversationInteractionRepository.findAll().size();

        // Create the UserConversationInteraction
        UserConversationInteractionDTO userConversationInteractionDTO = userConversationInteractionMapper.userConversationInteractionToUserConversationInteractionDTO(userConversationInteraction);

        restUserConversationInteractionMockMvc.perform(post("/api/userConversationInteractions")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(userConversationInteractionDTO)))
                .andExpect(status().isCreated());

        // Validate the UserConversationInteraction in the database
        List<UserConversationInteraction> userConversationInteractions = userConversationInteractionRepository.findAll();
        assertThat(userConversationInteractions).hasSize(databaseSizeBeforeCreate + 1);
        UserConversationInteraction testUserConversationInteraction = userConversationInteractions.get(userConversationInteractions.size() - 1);
        assertThat(testUserConversationInteraction.getArchived()).isEqualTo(DEFAULT_ARCHIVED);
        assertThat(testUserConversationInteraction.getFavorited()).isEqualTo(DEFAULT_FAVORITED);
        assertThat(testUserConversationInteraction.getIsRead()).isEqualTo(DEFAULT_IS_READ);
    }

    @Test
    @Transactional
    public void getAllUserConversationInteractions() throws Exception {
        // Initialize the database
        userConversationInteractionRepository.saveAndFlush(userConversationInteraction);

        // Get all the userConversationInteractions
        restUserConversationInteractionMockMvc.perform(get("/api/userConversationInteractions"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(userConversationInteraction.getId().intValue())))
                .andExpect(jsonPath("$.[*].archived").value(hasItem(DEFAULT_ARCHIVED.booleanValue())))
                .andExpect(jsonPath("$.[*].favorited").value(hasItem(DEFAULT_FAVORITED.booleanValue())))
                .andExpect(jsonPath("$.[*].isRead").value(hasItem(DEFAULT_IS_READ.booleanValue())));
    }

    @Test
    @Transactional
    public void getUserConversationInteraction() throws Exception {
        // Initialize the database
        userConversationInteractionRepository.saveAndFlush(userConversationInteraction);

        // Get the userConversationInteraction
        restUserConversationInteractionMockMvc.perform(get("/api/userConversationInteractions/{id}", userConversationInteraction.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(userConversationInteraction.getId().intValue()))
            .andExpect(jsonPath("$.archived").value(DEFAULT_ARCHIVED.booleanValue()))
            .andExpect(jsonPath("$.favorited").value(DEFAULT_FAVORITED.booleanValue()))
            .andExpect(jsonPath("$.isRead").value(DEFAULT_IS_READ.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingUserConversationInteraction() throws Exception {
        // Get the userConversationInteraction
        restUserConversationInteractionMockMvc.perform(get("/api/userConversationInteractions/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateUserConversationInteraction() throws Exception {
        // Initialize the database
        userConversationInteractionRepository.saveAndFlush(userConversationInteraction);

		int databaseSizeBeforeUpdate = userConversationInteractionRepository.findAll().size();

        // Update the userConversationInteraction
        userConversationInteraction.setArchived(UPDATED_ARCHIVED);
        userConversationInteraction.setFavorited(UPDATED_FAVORITED);
        userConversationInteraction.setIsRead(UPDATED_IS_READ);
        UserConversationInteractionDTO userConversationInteractionDTO = userConversationInteractionMapper.userConversationInteractionToUserConversationInteractionDTO(userConversationInteraction);

        restUserConversationInteractionMockMvc.perform(put("/api/userConversationInteractions")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(userConversationInteractionDTO)))
                .andExpect(status().isOk());

        // Validate the UserConversationInteraction in the database
        List<UserConversationInteraction> userConversationInteractions = userConversationInteractionRepository.findAll();
        assertThat(userConversationInteractions).hasSize(databaseSizeBeforeUpdate);
        UserConversationInteraction testUserConversationInteraction = userConversationInteractions.get(userConversationInteractions.size() - 1);
        assertThat(testUserConversationInteraction.getArchived()).isEqualTo(UPDATED_ARCHIVED);
        assertThat(testUserConversationInteraction.getFavorited()).isEqualTo(UPDATED_FAVORITED);
        assertThat(testUserConversationInteraction.getIsRead()).isEqualTo(UPDATED_IS_READ);
    }

    @Test
    @Transactional
    public void deleteUserConversationInteraction() throws Exception {
        // Initialize the database
        userConversationInteractionRepository.saveAndFlush(userConversationInteraction);

		int databaseSizeBeforeDelete = userConversationInteractionRepository.findAll().size();

        // Get the userConversationInteraction
        restUserConversationInteractionMockMvc.perform(delete("/api/userConversationInteractions/{id}", userConversationInteraction.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<UserConversationInteraction> userConversationInteractions = userConversationInteractionRepository.findAll();
        assertThat(userConversationInteractions).hasSize(databaseSizeBeforeDelete - 1);
    }
}
