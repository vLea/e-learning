package edu.ubb.elearning.web.rest;

import edu.ubb.elearning.Application;
import edu.ubb.elearning.domain.Discussion;
import edu.ubb.elearning.repository.DiscussionRepository;
import edu.ubb.elearning.service.DiscussionService;
import edu.ubb.elearning.web.rest.dto.DiscussionDTO;
import edu.ubb.elearning.web.rest.mapper.DiscussionMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the DiscussionResource REST controller.
 *
 * @see DiscussionResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class DiscussionResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAA";
    private static final String UPDATED_NAME = "BBBBB";

    @Inject
    private DiscussionRepository discussionRepository;

    @Inject
    private DiscussionService discussionService;
    
    @Inject
    private DiscussionMapper discussionMapper;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restDiscussionMockMvc;

    private Discussion discussion;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        DiscussionResource discussionResource = new DiscussionResource();
        ReflectionTestUtils.setField(discussionResource, "discussionService", discussionService);
        ReflectionTestUtils.setField(discussionResource, "discussionMapper", discussionMapper);
        this.restDiscussionMockMvc = MockMvcBuilders.standaloneSetup(discussionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        discussion = new Discussion();
        discussion.setName(DEFAULT_NAME);
    }

    @Test
    @Transactional
    public void createDiscussion() throws Exception {
        int databaseSizeBeforeCreate = discussionRepository.findAll().size();

        // Create the Discussion
        DiscussionDTO discussionDTO = discussionMapper.discussionToDiscussionDTO(discussion);

        restDiscussionMockMvc.perform(post("/api/discussions")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(discussionDTO)))
                .andExpect(status().isCreated());

        // Validate the Discussion in the database
        List<Discussion> discussions = discussionRepository.findAll();
        assertThat(discussions).hasSize(databaseSizeBeforeCreate + 1);
        Discussion testDiscussion = discussions.get(discussions.size() - 1);
        assertThat(testDiscussion.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = discussionRepository.findAll().size();
        // set the field null
        discussion.setName(null);

        // Create the Discussion, which fails.
        DiscussionDTO discussionDTO = discussionMapper.discussionToDiscussionDTO(discussion);

        restDiscussionMockMvc.perform(post("/api/discussions")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(discussionDTO)))
                .andExpect(status().isBadRequest());

        List<Discussion> discussions = discussionRepository.findAll();
        assertThat(discussions).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllDiscussions() throws Exception {
        // Initialize the database
        discussionRepository.saveAndFlush(discussion);

        // Get all the discussions
        restDiscussionMockMvc.perform(get("/api/discussions"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(discussion.getId().intValue())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }

    @Test
    @Transactional
    public void getDiscussion() throws Exception {
        // Initialize the database
        discussionRepository.saveAndFlush(discussion);

        // Get the discussion
        restDiscussionMockMvc.perform(get("/api/discussions/{id}", discussion.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(discussion.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingDiscussion() throws Exception {
        // Get the discussion
        restDiscussionMockMvc.perform(get("/api/discussions/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDiscussion() throws Exception {
        // Initialize the database
        discussionRepository.saveAndFlush(discussion);

		int databaseSizeBeforeUpdate = discussionRepository.findAll().size();

        // Update the discussion
        discussion.setName(UPDATED_NAME);
        DiscussionDTO discussionDTO = discussionMapper.discussionToDiscussionDTO(discussion);

        restDiscussionMockMvc.perform(put("/api/discussions")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(discussionDTO)))
                .andExpect(status().isOk());

        // Validate the Discussion in the database
        List<Discussion> discussions = discussionRepository.findAll();
        assertThat(discussions).hasSize(databaseSizeBeforeUpdate);
        Discussion testDiscussion = discussions.get(discussions.size() - 1);
        assertThat(testDiscussion.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    public void deleteDiscussion() throws Exception {
        // Initialize the database
        discussionRepository.saveAndFlush(discussion);

		int databaseSizeBeforeDelete = discussionRepository.findAll().size();

        // Get the discussion
        restDiscussionMockMvc.perform(delete("/api/discussions/{id}", discussion.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Discussion> discussions = discussionRepository.findAll();
        assertThat(discussions).hasSize(databaseSizeBeforeDelete - 1);
    }
}
