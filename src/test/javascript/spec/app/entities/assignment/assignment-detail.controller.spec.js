'use strict';

describe('Assignment Detail Controller', function() {
    var $scope, $rootScope;
    var MockEntity, MockAssignment, MockGroups, MockCourse;
    var createController;

    beforeEach(inject(function($injector) {
        $rootScope = $injector.get('$rootScope');
        $scope = $rootScope.$new();
        MockEntity = jasmine.createSpy('MockEntity');
        MockAssignment = jasmine.createSpy('MockAssignment');
        MockGroups = jasmine.createSpy('MockGroups');
        MockCourse = jasmine.createSpy('MockCourse');
        

        var locals = {
            '$scope': $scope,
            '$rootScope': $rootScope,
            'entity': MockEntity ,
            'Assignment': MockAssignment,
            'Groups': MockGroups,
            'Course': MockCourse
        };
        createController = function() {
            $injector.get('$controller')("AssignmentDetailController", locals);
        };
    }));


    describe('Root Scope Listening', function() {
        it('Unregisters root scope listener upon scope destruction', function() {
            var eventType = 'elearningApp:assignmentUpdate';

            createController();
            expect($rootScope.$$listenerCount[eventType]).toEqual(1);

            $scope.$destroy();
            expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
        });
    });
});
