'use strict';

describe('Calendar Detail Controller', function() {
    var $scope, $rootScope;
    var MockEntity, MockCalendar, MockUser, MockEvent;
    var createController;

    beforeEach(inject(function($injector) {
        $rootScope = $injector.get('$rootScope');
        $scope = $rootScope.$new();
        MockEntity = jasmine.createSpy('MockEntity');
        MockCalendar = jasmine.createSpy('MockCalendar');
        MockUser = jasmine.createSpy('MockUser');
        MockEvent = jasmine.createSpy('MockEvent');
        

        var locals = {
            '$scope': $scope,
            '$rootScope': $rootScope,
            'entity': MockEntity ,
            'Calendar': MockCalendar,
            'User': MockUser,
            'Event': MockEvent
        };
        createController = function() {
            $injector.get('$controller')("CalendarDetailController", locals);
        };
    }));


    describe('Root Scope Listening', function() {
        it('Unregisters root scope listener upon scope destruction', function() {
            var eventType = 'elearningApp:calendarUpdate';

            createController();
            expect($rootScope.$$listenerCount[eventType]).toEqual(1);

            $scope.$destroy();
            expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
        });
    });
});
