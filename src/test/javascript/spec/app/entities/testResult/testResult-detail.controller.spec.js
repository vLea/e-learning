'use strict';

describe('TestResult Detail Controller', function() {
    var $scope, $rootScope;
    var MockEntity, MockTestResult, MockUser, MockTests;
    var createController;

    beforeEach(inject(function($injector) {
        $rootScope = $injector.get('$rootScope');
        $scope = $rootScope.$new();
        MockEntity = jasmine.createSpy('MockEntity');
        MockTestResult = jasmine.createSpy('MockTestResult');
        MockUser = jasmine.createSpy('MockUser');
        MockTests = jasmine.createSpy('MockTests');
        

        var locals = {
            '$scope': $scope,
            '$rootScope': $rootScope,
            'entity': MockEntity ,
            'TestResult': MockTestResult,
            'User': MockUser,
            'Tests': MockTests
        };
        createController = function() {
            $injector.get('$controller')("TestResultDetailController", locals);
        };
    }));


    describe('Root Scope Listening', function() {
        it('Unregisters root scope listener upon scope destruction', function() {
            var eventType = 'elearningApp:testResultUpdate';

            createController();
            expect($rootScope.$$listenerCount[eventType]).toEqual(1);

            $scope.$destroy();
            expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
        });
    });
});
