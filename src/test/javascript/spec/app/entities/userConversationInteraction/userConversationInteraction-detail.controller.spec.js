'use strict';

describe('UserConversationInteraction Detail Controller', function() {
    var $scope, $rootScope;
    var MockEntity, MockUserConversationInteraction, MockUser, MockConversation;
    var createController;

    beforeEach(inject(function($injector) {
        $rootScope = $injector.get('$rootScope');
        $scope = $rootScope.$new();
        MockEntity = jasmine.createSpy('MockEntity');
        MockUserConversationInteraction = jasmine.createSpy('MockUserConversationInteraction');
        MockUser = jasmine.createSpy('MockUser');
        MockConversation = jasmine.createSpy('MockConversation');
        

        var locals = {
            '$scope': $scope,
            '$rootScope': $rootScope,
            'entity': MockEntity ,
            'UserConversationInteraction': MockUserConversationInteraction,
            'User': MockUser,
            'Conversation': MockConversation
        };
        createController = function() {
            $injector.get('$controller')("UserConversationInteractionDetailController", locals);
        };
    }));


    describe('Root Scope Listening', function() {
        it('Unregisters root scope listener upon scope destruction', function() {
            var eventType = 'elearningApp:userConversationInteractionUpdate';

            createController();
            expect($rootScope.$$listenerCount[eventType]).toEqual(1);

            $scope.$destroy();
            expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
        });
    });
});
