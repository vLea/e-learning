'use strict';

describe('Question Detail Controller', function() {
    var $scope, $rootScope;
    var MockEntity, MockQuestion, MockTests;
    var createController;

    beforeEach(inject(function($injector) {
        $rootScope = $injector.get('$rootScope');
        $scope = $rootScope.$new();
        MockEntity = jasmine.createSpy('MockEntity');
        MockQuestion = jasmine.createSpy('MockQuestion');
        MockTests = jasmine.createSpy('MockTests');
        

        var locals = {
            '$scope': $scope,
            '$rootScope': $rootScope,
            'entity': MockEntity ,
            'Question': MockQuestion,
            'Tests': MockTests
        };
        createController = function() {
            $injector.get('$controller')("QuestionDetailController", locals);
        };
    }));


    describe('Root Scope Listening', function() {
        it('Unregisters root scope listener upon scope destruction', function() {
            var eventType = 'elearningApp:questionUpdate';

            createController();
            expect($rootScope.$$listenerCount[eventType]).toEqual(1);

            $scope.$destroy();
            expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
        });
    });
});
