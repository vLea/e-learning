'use strict';

describe('Pages Detail Controller', function() {
    var $scope, $rootScope;
    var MockEntity, MockPages, MockGroups, MockFile;
    var createController;

    beforeEach(inject(function($injector) {
        $rootScope = $injector.get('$rootScope');
        $scope = $rootScope.$new();
        MockEntity = jasmine.createSpy('MockEntity');
        MockPages = jasmine.createSpy('MockPages');
        MockGroups = jasmine.createSpy('MockGroups');
        MockFile = jasmine.createSpy('MockFile');
        

        var locals = {
            '$scope': $scope,
            '$rootScope': $rootScope,
            'entity': MockEntity ,
            'Pages': MockPages,
            'Groups': MockGroups,
            'File': MockFile
        };
        createController = function() {
            $injector.get('$controller')("PagesDetailController", locals);
        };
    }));


    describe('Root Scope Listening', function() {
        it('Unregisters root scope listener upon scope destruction', function() {
            var eventType = 'elearningApp:pagesUpdate';

            createController();
            expect($rootScope.$$listenerCount[eventType]).toEqual(1);

            $scope.$destroy();
            expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
        });
    });
});
