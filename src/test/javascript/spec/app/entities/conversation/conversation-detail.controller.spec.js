'use strict';

describe('Conversation Detail Controller', function() {
    var $scope, $rootScope;
    var MockEntity, MockConversation, MockCourse, MockGroups, MockConversationComment;
    var createController;

    beforeEach(inject(function($injector) {
        $rootScope = $injector.get('$rootScope');
        $scope = $rootScope.$new();
        MockEntity = jasmine.createSpy('MockEntity');
        MockConversation = jasmine.createSpy('MockConversation');
        MockCourse = jasmine.createSpy('MockCourse');
        MockGroups = jasmine.createSpy('MockGroups');
        MockConversationComment = jasmine.createSpy('MockConversationComment');
        

        var locals = {
            '$scope': $scope,
            '$rootScope': $rootScope,
            'entity': MockEntity ,
            'Conversation': MockConversation,
            'Course': MockCourse,
            'Groups': MockGroups,
            'ConversationComment': MockConversationComment
        };
        createController = function() {
            $injector.get('$controller')("ConversationDetailController", locals);
        };
    }));


    describe('Root Scope Listening', function() {
        it('Unregisters root scope listener upon scope destruction', function() {
            var eventType = 'elearningApp:conversationUpdate';

            createController();
            expect($rootScope.$$listenerCount[eventType]).toEqual(1);

            $scope.$destroy();
            expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
        });
    });
});
