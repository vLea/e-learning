'use strict';

describe('Announcement Detail Controller', function() {
    var $scope, $rootScope;
    var MockEntity, MockAnnouncement, MockFile, MockGroups;
    var createController;

    beforeEach(inject(function($injector) {
        $rootScope = $injector.get('$rootScope');
        $scope = $rootScope.$new();
        MockEntity = jasmine.createSpy('MockEntity');
        MockAnnouncement = jasmine.createSpy('MockAnnouncement');
        MockFile = jasmine.createSpy('MockFile');
        MockGroups = jasmine.createSpy('MockGroups');
        

        var locals = {
            '$scope': $scope,
            '$rootScope': $rootScope,
            'entity': MockEntity ,
            'Announcement': MockAnnouncement,
            'File': MockFile,
            'Groups': MockGroups
        };
        createController = function() {
            $injector.get('$controller')("AnnouncementDetailController", locals);
        };
    }));


    describe('Root Scope Listening', function() {
        it('Unregisters root scope listener upon scope destruction', function() {
            var eventType = 'elearningApp:announcementUpdate';

            createController();
            expect($rootScope.$$listenerCount[eventType]).toEqual(1);

            $scope.$destroy();
            expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
        });
    });
});
