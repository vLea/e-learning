'use strict';

describe('Message Detail Controller', function() {
    var $scope, $rootScope;
    var MockEntity, MockMessage, MockConversation, MockUser, MockFile;
    var createController;

    beforeEach(inject(function($injector) {
        $rootScope = $injector.get('$rootScope');
        $scope = $rootScope.$new();
        MockEntity = jasmine.createSpy('MockEntity');
        MockMessage = jasmine.createSpy('MockMessage');
        MockConversation = jasmine.createSpy('MockConversation');
        MockUser = jasmine.createSpy('MockUser');
        MockFile = jasmine.createSpy('MockFile');
        

        var locals = {
            '$scope': $scope,
            '$rootScope': $rootScope,
            'entity': MockEntity ,
            'Message': MockMessage,
            'Conversation': MockConversation,
            'User': MockUser,
            'File': MockFile
        };
        createController = function() {
            $injector.get('$controller')("MessageDetailController", locals);
        };
    }));


    describe('Root Scope Listening', function() {
        it('Unregisters root scope listener upon scope destruction', function() {
            var eventType = 'elearningApp:messageUpdate';

            createController();
            expect($rootScope.$$listenerCount[eventType]).toEqual(1);

            $scope.$destroy();
            expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
        });
    });
});
