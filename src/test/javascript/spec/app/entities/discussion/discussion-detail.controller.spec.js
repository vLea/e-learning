'use strict';

describe('Discussion Detail Controller', function() {
    var $scope, $rootScope;
    var MockEntity, MockDiscussion, MockGroups;
    var createController;

    beforeEach(inject(function($injector) {
        $rootScope = $injector.get('$rootScope');
        $scope = $rootScope.$new();
        MockEntity = jasmine.createSpy('MockEntity');
        MockDiscussion = jasmine.createSpy('MockDiscussion');
        MockGroups = jasmine.createSpy('MockGroups');
        

        var locals = {
            '$scope': $scope,
            '$rootScope': $rootScope,
            'entity': MockEntity ,
            'Discussion': MockDiscussion,
            'Groups': MockGroups
        };
        createController = function() {
            $injector.get('$controller')("DiscussionDetailController", locals);
        };
    }));


    describe('Root Scope Listening', function() {
        it('Unregisters root scope listener upon scope destruction', function() {
            var eventType = 'elearningApp:discussionUpdate';

            createController();
            expect($rootScope.$$listenerCount[eventType]).toEqual(1);

            $scope.$destroy();
            expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
        });
    });
});
