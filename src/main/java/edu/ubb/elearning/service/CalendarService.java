package edu.ubb.elearning.service;

import edu.ubb.elearning.domain.Calendar;
import edu.ubb.elearning.repository.CalendarRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.inject.Inject;

/**
 * Service class for managing Calendar.
 */
@Service
@Transactional
public class CalendarService {

    private final Logger log = LoggerFactory.getLogger(CalendarService.class);

    @Inject
    private CalendarRepository calendarRepository;

    public Calendar createCalendar(Calendar calendar) throws RuntimeException {
        log.debug("Service method to save Calendar : {}", calendar);
        if (calendar.getId() != null) {
            throw new RuntimeException("Failure: A new calendar cannot already have an ID");
        }
        Calendar result = calendarRepository.save(calendar);
        return result;
    }

    public Calendar updateCalendar(Calendar calendar) {
        log.debug("Service method to update Calendar : {}", calendar);
        if (calendar.getId() == null) {
            return createCalendar(calendar);
        }
        Calendar result = calendarRepository.save(calendar);
        return result;
    }

    @Transactional(readOnly = true)
    public Page<Calendar> getAllCalendars(Pageable pageable){
        Page<Calendar> page = calendarRepository.findAll(pageable);
        return page;
    }

    public Calendar getCalendar(Long id) {
        log.debug("Service method to get Calendar : {}", id);
        return calendarRepository.findOneWithEagerRelationships(id);
    }

    public void deleteCalendar(Long id) {
        log.debug("Service method to delete Calendar : {}", id);
        calendarRepository.delete(id);
    }
}
