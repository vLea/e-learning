package edu.ubb.elearning.service;

import edu.ubb.elearning.domain.Assignment;
import edu.ubb.elearning.repository.AssignmentRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.inject.Inject;

/**
 * Service class for managing Assignment.
 */
@Service
@Transactional
public class AssignmentService {

    private final Logger log = LoggerFactory.getLogger(AssignmentService.class);

    @Inject
    private AssignmentRepository assignmentRepository;

    public Assignment createAssignment(Assignment assignment) throws RuntimeException {
        log.debug("Service method to save Assignment : {}", assignment);
        if (assignment.getId() != null) {
            throw new RuntimeException("Failure: A new assignment cannot already have an ID");
        }
        Assignment result = assignmentRepository.save(assignment);
        return result;
    }

    public Assignment updateAssignment(Assignment assignment) {
        log.debug("Service method to update Assignment : {}", assignment);
        if (assignment.getId() == null) {
            return createAssignment(assignment);
        }
        Assignment result = assignmentRepository.save(assignment);
        return result;
    }

    @Transactional(readOnly = true)
    public Page<Assignment> getAllAssignments(Pageable pageable){
        Page<Assignment> page = assignmentRepository.findAll(pageable);
        return page;
    }

    public Assignment getAssignment(Long id) {
        log.debug("Service method to get Assignment : {}", id);
        return assignmentRepository.findOne(id);
    }

    public void deleteAssignment(Long id) {
        log.debug("Service method to delete Assignment : {}", id);
        assignmentRepository.delete(id);
    }
}
