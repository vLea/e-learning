package edu.ubb.elearning.service;

import edu.ubb.elearning.domain.File;
import edu.ubb.elearning.repository.FileRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.inject.Inject;

/**
 * Service class for managing File.
 */
@Service
@Transactional
public class FileService {

    private final Logger log = LoggerFactory.getLogger(FileService.class);

    @Inject
    private FileRepository fileRepository;

    public File createFile(File file) throws RuntimeException {
        log.debug("Service method to save File : {}", file);
        if (file.getId() != null) {
            throw new RuntimeException("Failure: A new file cannot already have an ID");
        }
        File result = fileRepository.save(file);
        return result;
    }

    public File updateFile(File file){
        log.debug("Service method to update File : {}", file);
        if (file.getId() == null) {
            return createFile(file);
        }
        File result = fileRepository.save(file);
        return result;
    }

    public Page<File> getAllFiles(Pageable pageable){
        Page<File> page = fileRepository.findAll(pageable);
        return page;
    }

    public File getFile(Long id) {
        log.debug("Service method to get File : {}", id);
        return fileRepository.findOne(id);
    }

    public void deleteFile(Long id) {
        log.debug("Service method to delete File : {}", id);
        fileRepository.delete(id);
    }
}
