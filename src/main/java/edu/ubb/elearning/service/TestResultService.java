package edu.ubb.elearning.service;

import edu.ubb.elearning.domain.TestResult;
import edu.ubb.elearning.repository.TestResultRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.inject.Inject;

/**
 * Service controller for managing TestResult.
 */
@Service
@Transactional
public class TestResultService {

    private final Logger log = LoggerFactory.getLogger(TestResultService.class);

    @Inject
    private TestResultRepository testResultRepository;

    public TestResult createTestResult(TestResult testResult) throws RuntimeException {
        log.debug("Service method to save TestResult : {}", testResult);
        if (testResult.getId() != null) {
            throw new RuntimeException("Failure: A new testResult cannot already have an ID");
        }
        TestResult result = testResultRepository.save(testResult);
        return result;
    }

    public TestResult updateTestResult(TestResult testResult) {
        log.debug("Service method to update TestResult : {}", testResult);
        if (testResult.getId() == null) {
            return createTestResult(testResult);
        }
        TestResult result = testResultRepository.save(testResult);
        return result;
    }

    public Page<TestResult> getAllTestResults(Pageable pageable) {
        Page<TestResult> page = testResultRepository.findAll(pageable);
        return page;
    }

    public TestResult getTestResult(Long id) {
        log.debug("Service method to get TestResult : {}", id);
        return testResultRepository.findOne(id);
    }

    public void deleteTestResult(Long id) {
        log.debug("Service method to delete TestResult : {}", id);
        testResultRepository.delete(id);
    }
}
