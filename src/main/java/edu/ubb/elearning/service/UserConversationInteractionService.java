package edu.ubb.elearning.service;

import edu.ubb.elearning.domain.UserConversationInteraction;
import edu.ubb.elearning.repository.UserConversationInteractionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.inject.Inject;

/**
 * Service class for managing UserConversationInteraction.
 */
@Service
@Transactional
public class UserConversationInteractionService {

    private final Logger log = LoggerFactory.getLogger(UserConversationInteractionService.class);

    @Inject
    private UserConversationInteractionRepository userConversationInteractionRepository;

    public UserConversationInteraction createUserConversationInteraction(UserConversationInteraction userConversationInteraction) throws RuntimeException {
        log.debug("Service method to save UserConversationInteraction : {}", userConversationInteraction);
        if (userConversationInteraction.getId() != null) {
            throw new RuntimeException("Failure: A new userConversationInteraction cannot already have an ID");
        }
        UserConversationInteraction result = userConversationInteractionRepository.save(userConversationInteraction);
        return result;
    }

    public UserConversationInteraction updateUserConversationInteraction(UserConversationInteraction userConversationInteraction) {
        log.debug("Service method to update UserConversationInteraction : {}", userConversationInteraction);
        if (userConversationInteraction.getId() == null) {
            return createUserConversationInteraction(userConversationInteraction);
        }
        UserConversationInteraction result = userConversationInteractionRepository.save(userConversationInteraction);
        return result; 
    }

    public Page<UserConversationInteraction> getAllUserConversationInteractions(Pageable pageable){
        Page<UserConversationInteraction> page = userConversationInteractionRepository.findAll(pageable);
        return page;
    }

    public UserConversationInteraction getUserConversationInteraction(Long id) {
        log.debug("Service method to get UserConversationInteraction : {}", id);
        return userConversationInteractionRepository.findOne(id);
    }

    public void deleteUserConversationInteraction(Long id) {
        log.debug("Service method to delete UserConversationInteraction : {}", id);
        userConversationInteractionRepository.delete(id);
    }
}
