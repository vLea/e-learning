package edu.ubb.elearning.service;

import edu.ubb.elearning.domain.Conversation;
import edu.ubb.elearning.repository.ConversationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.inject.Inject;

/**
 * Service class for managing Conversation.
 */
@Service
@Transactional
public class ConversationService {

    private final Logger log = LoggerFactory.getLogger(ConversationService.class);

    @Inject
    private ConversationRepository conversationRepository;

    public Conversation createConversation(Conversation conversation) throws RuntimeException {
        log.debug("Service method to save Conversation : {}", conversation);
        if (conversation.getId() != null) {
            throw new RuntimeException("Failure: A new conversation cannot already have an ID");
        }
        Conversation result = conversationRepository.save(conversation);
        return result;
    }

    public Conversation updateConversation(Conversation conversation){
        log.debug("Service method to update Conversation : {}", conversation);
        if (conversation.getId() == null) {
            return createConversation(conversation);
        }
        Conversation result = conversationRepository.save(conversation);
        return result;
    }

    @Transactional(readOnly = true)
    public Page<Conversation> getAllConversations(Pageable pageable) {
        Page<Conversation> page = conversationRepository.findAll(pageable);
        return page;
    }

    public Conversation getConversation(Long id) {
        log.debug("Service method to get Conversation : {}", id);
        return conversationRepository.findOneWithEagerRelationships(id);
    }

    public void deleteConversation(Long id) {
        log.debug("Service method to delete Conversation : {}", id);
        conversationRepository.delete(id);
    }
}
