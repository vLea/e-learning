package edu.ubb.elearning.service;

import edu.ubb.elearning.domain.Groups;
import edu.ubb.elearning.repository.GroupsRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.inject.Inject;

/**
 * Service class for managing Groups.
 */
@Service
@Transactional
public class GroupsService {

    private final Logger log = LoggerFactory.getLogger(GroupsService.class);

    @Inject
    private GroupsRepository groupsRepository;

    public Groups createGroups(Groups groups) throws RuntimeException {
        log.debug("Service method to save Groups : {}", groups);
        if (groups.getId() != null) {
            throw new RuntimeException("Failure: A new groups cannot already have an ID");
        }
        Groups result = groupsRepository.save(groups);
        return result;
    }

    public Groups updateGroups(Groups groups){
        log.debug("Service method to update Groups : {}", groups);
        if (groups.getId() == null) {
            return createGroups(groups);
        }
        Groups result = groupsRepository.save(groups);
        return result;
    }

    public Page<Groups> getAllGroupss(Pageable pageable){
        Page<Groups> page = groupsRepository.findAll(pageable);
        return page;
    }

    public Groups getGroups(Long id) {
        log.debug("Service method to get Groups : {}", id);
        return groupsRepository.findOneWithEagerRelationships(id);
    }

    public void deleteGroups(Long id) {
        log.debug("Service method to delete Groups : {}", id);
        groupsRepository.delete(id);
    }
}
