package edu.ubb.elearning.service;

import edu.ubb.elearning.domain.Event;
import edu.ubb.elearning.repository.EventRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.inject.Inject;

/**
 * Service class for managing Event.
 */
@Service
@Transactional
public class EventService {

    private final Logger log = LoggerFactory.getLogger(EventService.class);

    @Inject
    private EventRepository eventRepository;

    public Event createEvent(Event event) throws RuntimeException {
        log.debug("Service method to save Event : {}", event);
        if (event.getId() != null) {
            throw new RuntimeException("Failure: A new event cannot already have an ID");
        }
        Event result = eventRepository.save(event);
        return result;
    }

    public Event updateEvent(Event event) {
        log.debug("Service method to update Event : {}", event);
        if (event.getId() == null) {
            return createEvent(event);
        }
        Event result = eventRepository.save(event);
        return result;
    }

    public Page<Event> getAllEvents(Pageable pageable){
        Page<Event> page = eventRepository.findAll(pageable);
        return page;
    }

    public Event getEvent(Long id) {
        log.debug("Service method to get Event : {}", id);
        return eventRepository.findOne(id);
    }

    public void deleteEvent(Long id) {
        log.debug("Service method to delete Event : {}", id);
        eventRepository.delete(id);
    }
}
