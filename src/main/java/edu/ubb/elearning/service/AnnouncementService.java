package edu.ubb.elearning.service;

import edu.ubb.elearning.domain.Announcement;
import edu.ubb.elearning.repository.AnnouncementRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.inject.Inject;

/**
 * Service class for managing Announcement.
 */
@Service
@Transactional
public class AnnouncementService {

    private final Logger log = LoggerFactory.getLogger(AnnouncementService.class);

    @Inject
    private AnnouncementRepository announcementRepository;

    public Announcement createAnnouncement(Announcement announcement) throws RuntimeException {
        log.debug("Service method to save Announcement : {}", announcement);
        if (announcement.getId() != null) {
            throw new RuntimeException("Failure: A new announcement cannot already have an ID");
        }
        Announcement result = announcementRepository.save(announcement);
        return result;
    }

    public Announcement updateAnnouncement(Announcement announcement) {
        log.debug("Service method to update Announcement : {}", announcement);
        if (announcement.getId() == null) {
            return createAnnouncement(announcement);
        }
        Announcement result = announcementRepository.save(announcement);
        return result;
    }

    @Transactional(readOnly = true)
    public Page<Announcement> getAllAnnouncements(Pageable pageable) {
        Page<Announcement> page = announcementRepository.findAll(pageable);
        return page;
    }

    public Announcement getAnnouncement(Long id) {
        log.debug("Service method to get Announcement : {}", id);
        return announcementRepository.findOneWithEagerRelationships(id);
    }

    public void deleteAnnouncement(Long id) {
        log.debug("Service method to delete Announcement : {}", id);
        announcementRepository.delete(id);
    }
}
