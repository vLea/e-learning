package edu.ubb.elearning.service;

import edu.ubb.elearning.domain.ConversationComment;
import edu.ubb.elearning.repository.ConversationCommentRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.inject.Inject;

/**
 * Service class for managing ConversationComment.
 */
@Service
@Transactional
public class ConversationCommentService {

    private final Logger log = LoggerFactory.getLogger(ConversationCommentService.class);

    @Inject
    private ConversationCommentRepository conversationCommentRepository;

    public ConversationComment createConversationComment(ConversationComment conversationComment) throws RuntimeException {
        log.debug("Service method to save ConversationComment : {}", conversationComment);
        if (conversationComment.getId() != null) {
            throw new RuntimeException("Failure: A new conversationComment cannot already have an ID");
        }
        ConversationComment result = conversationCommentRepository.save(conversationComment);
        return result;
    }

    public ConversationComment updateConversationComment(ConversationComment conversationComment) {
        log.debug("Service method to update ConversationComment : {}", conversationComment);
        if (conversationComment.getId() == null) {
            return createConversationComment(conversationComment);
        }
        ConversationComment result = conversationCommentRepository.save(conversationComment);
        return result;
    }

    @Transactional(readOnly = true)
    public Page<ConversationComment> getAllConversationComments(Pageable pageable) {
        Page<ConversationComment> page = conversationCommentRepository.findAll(pageable);
        return page;
    }

    public ConversationComment getConversationComment(Long id) {
        log.debug("Service method to get ConversationComment : {}", id);
        return conversationCommentRepository.findOne(id);
    }

    public void deleteConversationComment(Long id) {
        log.debug("Service method to delete ConversationComment : {}", id);
        conversationCommentRepository.delete(id);
    }
}
