package edu.ubb.elearning.service;

import edu.ubb.elearning.domain.Discussion;
import edu.ubb.elearning.repository.DiscussionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.inject.Inject;

/**
 * Service class for managing Discussion.
 */
@Service
@Transactional
public class DiscussionService {

    private final Logger log = LoggerFactory.getLogger(DiscussionService.class);

    @Inject
    private DiscussionRepository discussionRepository;

    public Discussion createDiscussion(Discussion discussion) throws RuntimeException {
        log.debug("Service method to save Discussion : {}", discussion);
        if (discussion.getId() != null) {
            throw new RuntimeException("Failure: A new discussion cannot already have an ID");
        }
        Discussion result = discussionRepository.save(discussion);
        return result;
    }

    public Discussion updateDiscussion(Discussion discussion) {
        log.debug("Service method to update Discussion : {}", discussion);
        if (discussion.getId() == null) {
            return createDiscussion(discussion);
        }
        Discussion result = discussionRepository.save(discussion);
        return result;
    }

    public Page<Discussion> getAllDiscussions(Pageable pageable){
        Page<Discussion> page = discussionRepository.findAll(pageable);
        return page;
    }

    public Discussion getDiscussion(Long id) {
        log.debug("Service method to get Discussion : {}", id);
        return discussionRepository.findOne(id);
    }

    public void deleteDiscussion(Long id) {
        log.debug("Service method to delete Discussion : {}", id);
        discussionRepository.delete(id);
    }
}
