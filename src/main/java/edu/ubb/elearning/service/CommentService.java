package edu.ubb.elearning.service;

import edu.ubb.elearning.domain.Comment;
import edu.ubb.elearning.repository.CommentRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.inject.Inject;

/**
 * Service method for managing Comment.
 */
@Service
@Transactional
public class CommentService {

    private final Logger log = LoggerFactory.getLogger(CommentService.class);

    @Inject
    private CommentRepository commentRepository;

    public Comment createComment(Comment comment) throws RuntimeException {
        log.debug("Service method to save Comment : {}", comment);
        if (comment.getId() != null) {
            throw new RuntimeException("Failure: A new comment cannot already have an ID");
        }
        Comment result = commentRepository.save(comment);
        return result;
    }

    public Comment updateComment(Comment comment){
        log.debug("Service method to update Comment : {}", comment);
        if (comment.getId() == null) {
            return createComment(comment);
        }
        Comment result = commentRepository.save(comment);
        return result;
    }

    @Transactional(readOnly = true)
    public Page<Comment> getAllComments(Pageable pageable){
        Page<Comment> page = commentRepository.findAll(pageable);
        return page;
    }

    public Comment getComment(Long id) {
        log.debug("Service method to get Comment : {}", id);
        return commentRepository.findOne(id);
    }

    public void deleteComment(Long id) {
        log.debug("Service method to delete Comment : {}", id);
        commentRepository.delete(id);
    }
}
