package edu.ubb.elearning.service;

import edu.ubb.elearning.domain.Pages;
import edu.ubb.elearning.repository.PagesRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.inject.Inject;

/**
 * Service class for managing Pages.
 */
@Service
@Transactional
public class PagesService {

    private final Logger log = LoggerFactory.getLogger(PagesService.class);

    @Inject
    private PagesRepository pagesRepository;

    public Pages createPages(Pages pages) throws RuntimeException {
        log.debug("Service method to save Pages : {}", pages);
        if (pages.getId() != null) {
            throw new RuntimeException("Failur: A new pages cannot already have an ID");
        }
        Pages result = pagesRepository.save(pages);
        return result;
    }

    public Pages updatePages(Pages pages){
        log.debug("Service method to update Pages : {}", pages);
        if (pages.getId() == null) {
            return createPages(pages);
        }
        Pages result = pagesRepository.save(pages);
        return result;
    }

    public Page<Pages> getAllPagess(Pageable pageable) {
        Page<Pages> page = pagesRepository.findAll(pageable);
        return page;
    }

    public Pages getPages(Long id) {
        log.debug("Service method to get Pages : {}", id);
        return pagesRepository.findOneWithEagerRelationships(id);
    }

    public void deletePages(Long id) {
        log.debug("Service method to delete Pages : {}", id);
        pagesRepository.delete(id);
    }
}
