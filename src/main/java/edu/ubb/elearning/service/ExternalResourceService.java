package edu.ubb.elearning.service;

import edu.ubb.elearning.domain.ExternalResource;
import edu.ubb.elearning.repository.ExternalResourceRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.inject.Inject;

/**
 * Service class for managing ExternalResource.
 */
@Service
@Transactional
public class ExternalResourceService {

    private final Logger log = LoggerFactory.getLogger(ExternalResourceService.class);

    @Inject
    private ExternalResourceRepository externalResourceRepository;

    public ExternalResource createExternalResource(ExternalResource externalResource) throws RuntimeException {
        log.debug("Service method to save ExternalResource : {}", externalResource);
        if (externalResource.getId() != null) {
            throw new RuntimeException("Failure: A new externalResource cannot already have an ID");
        }
        ExternalResource result = externalResourceRepository.save(externalResource);
        return result;
    }

    public ExternalResource updateExternalResource(ExternalResource externalResource) {
        log.debug("Service method to update ExternalResource : {}", externalResource);
        if (externalResource.getId() == null) {
            return createExternalResource(externalResource);
        }
        ExternalResource result = externalResourceRepository.save(externalResource);
        return result;
    }

    public Page<ExternalResource> getAllExternalResources(Pageable pageable){
        Page<ExternalResource> page = externalResourceRepository.findAll(pageable);
        return page;
    }

    public ExternalResource getExternalResource(Long id) {
        log.debug("Service method to get ExternalResource : {}", id);
        return externalResourceRepository.findOne(id);
    }

    public void deleteExternalResource(Long id) {
        log.debug("Service method to delete ExternalResource : {}", id);
        externalResourceRepository.delete(id);
    }
}
