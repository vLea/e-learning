package edu.ubb.elearning.service;

import edu.ubb.elearning.domain.Tests;
import edu.ubb.elearning.repository.TestsRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.inject.Inject;

/**
 * Service class for managing Tests.
 */
@Service
@Transactional
public class TestsService {

    private final Logger log = LoggerFactory.getLogger(TestsService.class);

    @Inject
    private TestsRepository testsRepository;

    public Tests createTests(Tests tests) throws RuntimeException {
        log.debug("Service method to save Tests : {}", tests);
        if (tests.getId() != null) {
            throw new RuntimeException("Failure: A new tests cannot already have an ID");
        }
        Tests result = testsRepository.save(tests);
        return result;
    }

    public Tests updateTests(Tests tests){
        log.debug("Service method to update Tests : {}", tests);
        if (tests.getId() == null) {
            return createTests(tests);
        }
        Tests result = testsRepository.save(tests);
        return result;
    }

    public Page<Tests> getAllTestss(Pageable pageable) {
        Page<Tests> page = testsRepository.findAll(pageable);
        return page;
    }

    public Tests getTests(Long id) {
        log.debug("Service method to get Tests : {}", id);
        return testsRepository.findOne(id);
    }

    public void deleteTests(Long id) {
        log.debug("Service method to delete Tests : {}", id);
        testsRepository.delete(id);
    }
}
