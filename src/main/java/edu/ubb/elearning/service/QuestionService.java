package edu.ubb.elearning.service;

import edu.ubb.elearning.domain.Question;
import edu.ubb.elearning.repository.QuestionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.inject.Inject;

/**
 * Service class for managing Question.
 */
@Service
@Transactional
public class QuestionService {

    private final Logger log = LoggerFactory.getLogger(QuestionService.class);

    @Inject
    private QuestionRepository questionRepository;

    public Question createQuestion(Question question) throws RuntimeException {
        log.debug("Service method to save Question : {}", question);
        if (question.getId() != null) {
            throw new RuntimeException("Failure: A new question cannot already have an ID");
        }
        Question result = questionRepository.save(question);
        return result;
    }

    public Question updateQuestion(Question question) {
        log.debug("Service method to update Question : {}", question);
        if (question.getId() == null) {
            return createQuestion(question);
        }
        Question result = questionRepository.save(question);
        return result;
    }

    public Page<Question> getAllQuestions(Pageable pageable) {
        Page<Question> page = questionRepository.findAll(pageable);
        return page;
    }

    public Question getQuestion(Long id) {
        log.debug("Service method to get Question : {}", id);
        return questionRepository.findOne(id);
    }

    public void deleteQuestion(Long id) {
        log.debug("Service method to delete Question : {}", id);
        questionRepository.delete(id);
    }
}
