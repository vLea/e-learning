package edu.ubb.elearning.service;

import edu.ubb.elearning.domain.Course;
import edu.ubb.elearning.repository.CourseRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.inject.Inject;

/**
 * Service class for managing Course.
 */
@Service
@Transactional
public class CourseService {

    private final Logger log = LoggerFactory.getLogger(CourseService.class);

    @Inject
    private CourseRepository courseRepository;

    public Course createCourse(Course course) throws RuntimeException {
        log.debug("Service method to save Course : {}", course);
        if (course.getId() != null) {
            throw new RuntimeException("Failure: A new course cannot already have an ID");
        }
        Course result = courseRepository.save(course);
        return result;
    }

    public Course updateCourse(Course course) {
        log.debug("Service method to update Course : {}", course);
        if (course.getId() == null) {
            return createCourse(course);
        }
        Course result = courseRepository.save(course);
        return result;
    }

    @Transactional(readOnly = true)
    public Page<Course> getAllCourses(Pageable pageable) {
        Page<Course> page = courseRepository.findAll(pageable);
        return page;
    }

    public Course getCourse(Long id) {
        log.debug("Service method to get Course : {}", id);
        return courseRepository.findOneWithEagerRelationships(id);
    }

    public void deleteCourse(Long id) {
        log.debug("Service method to delete Course : {}", id);
        courseRepository.delete(id);
    }
}
