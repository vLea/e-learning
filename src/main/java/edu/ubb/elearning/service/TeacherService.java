package edu.ubb.elearning.service;

import edu.ubb.elearning.domain.Teacher;
import edu.ubb.elearning.repository.TeacherRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;

/**
 * Service class for managing Teacher.
 */
@Service
@Transactional
public class TeacherService {

    private final Logger log = LoggerFactory.getLogger(TeacherService.class);

    @Inject
    private TeacherRepository teacherRepository;

    public Teacher createTeacher(Teacher teacher) throws RuntimeException {
        log.debug("Service method to save Teacher : {}", teacher);
        if (teacher.getId() != null) {
            throw new RuntimeException("Failure: A new teacher cannot already have an ID");
        }
        Teacher result = teacherRepository.save(teacher);
        return result;
    }

    public Teacher updateTeacher(Teacher teacher){
        log.debug("Service method to update Teacher : {}", teacher);
        if (teacher.getId() == null) {
            return createTeacher(teacher);
        }
        Teacher result = teacherRepository.save(teacher);
        return result;
    }

    public Page<Teacher> getAllTeachers(Pageable pageable){
        Page<Teacher> page = teacherRepository.findAll(pageable);
        return page;
    }

    public Teacher getTeacher(Long id) {
        log.debug("Service method to get Teacher : {}", id);
        return teacherRepository.findOne(id);
    }

    public void deleteTeacher(Long id) {
        log.debug("Service method to delete Teacher : {}", id);
        teacherRepository.delete(id);
    }
}
