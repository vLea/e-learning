package edu.ubb.elearning.service;

import edu.ubb.elearning.domain.Message;
import edu.ubb.elearning.repository.MessageRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.inject.Inject;

/**
 * Service class for managing Message.
 */
@Service
@Transactional
public class MessageService {

    private final Logger log = LoggerFactory.getLogger(MessageService.class);

    @Inject
    private MessageRepository messageRepository;

    public Message createMessage(Message message) throws RuntimeException {
        log.debug("Service method to save Message : {}", message);
        if (message.getId() != null) {
            throw new RuntimeException("Failure: A new message cannot already have an ID");
        }
        Message result = messageRepository.save(message);
        return result;
    }

    public Message updateMessage(Message message){
        log.debug("Service method to update Message : {}", message);
        if (message.getId() == null) {
            return createMessage(message);
        }
        Message result = messageRepository.save(message);
    	return result;
    }

    public Page<Message> getAllMessages(Pageable pageable) {
        Page<Message> page = messageRepository.findAll(pageable);
        return page;
    }

    public Message getMessage(Long id) {
        log.debug("Service method to get Message : {}", id);
        return messageRepository.findOneWithEagerRelationships(id);
    }

    public void deleteMessage(Long id) {
        log.debug("Service method to delete Message : {}", id);
        messageRepository.delete(id);
    }
}
