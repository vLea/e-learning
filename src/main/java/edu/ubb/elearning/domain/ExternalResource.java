package edu.ubb.elearning.domain;


import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A ExternalResource.
 */
@Entity
@Table(name = "external_resource")
public class ExternalResource implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "type")
    private String type;

    @Lob
    @Column(name = "data")
    private byte[] data;


    @Column(name = "data_content_type")
    private String dataContentType;
    @Column(name = "path")
    private String path;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    public String getDataContentType() {
        return dataContentType;
    }

    public void setDataContentType(String dataContentType) {
        this.dataContentType = dataContentType;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ExternalResource externalResource = (ExternalResource) o;
        return Objects.equals(id, externalResource.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "ExternalResource{" +
            "id=" + id +
            ", name='" + name + "'" +
            ", type='" + type + "'" +
            ", data='" + data + "'" +
            ", dataContentType='" + dataContentType + "'" +
            ", path='" + path + "'" +
            '}';
    }
}
