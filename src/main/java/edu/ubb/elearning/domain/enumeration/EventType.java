package edu.ubb.elearning.domain.enumeration;

/**
 * The EventType enumeration.
 */
public enum EventType {
    TYPE1, TYPE2, TYPE3
}
