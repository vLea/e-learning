package edu.ubb.elearning.domain.enumeration;

/**
 * The TestType enumeration.
 */
public enum TestType {
    TYPE1,TYPE2,TYPE3
}
