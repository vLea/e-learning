package edu.ubb.elearning.web.rest.mapper;

import edu.ubb.elearning.domain.*;
import edu.ubb.elearning.web.rest.dto.GroupsDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Groups and its DTO GroupsDTO.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class, })
public interface GroupsMapper {

    GroupsDTO groupsToGroupsDTO(Groups groups);

    Groups groupsDTOToGroups(GroupsDTO groupsDTO);

    
    default User userFromId(Long id) {
        if (id == null) {
            return null;
        }
        User user = new User();
        user.setId(id);
        return user;
    }
}
