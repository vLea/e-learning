package edu.ubb.elearning.web.rest.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;


/**
 * A DTO for the TestResult entity.
 */
public class TestResultDTO implements Serializable {

    private Long id;

    @Min(value = 1)
    @Max(value = 10)
    private Double mark;

    private Long userId;

    private String userLogin;

    private Long testsId;

    private String testsName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getMark() {
        return mark;
    }

    public void setMark(Double mark) {
        this.mark = mark;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public Long getTestsId() {
        return testsId;
    }

    public void setTestsId(Long testsId) {
        this.testsId = testsId;
    }

    public String getTestsName() {
        return testsName;
    }

    public void setTestsName(String testsName) {
        this.testsName = testsName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        TestResultDTO testResultDTO = (TestResultDTO) o;

        if ( ! Objects.equals(id, testResultDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "TestResultDTO{" +
            "id=" + id +
            ", mark='" + mark + "'" +
            '}';
    }
}
