package edu.ubb.elearning.web.rest.mapper;

import edu.ubb.elearning.domain.*;
import edu.ubb.elearning.web.rest.dto.FileDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity File and its DTO FileDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface FileMapper {

    FileDTO fileToFileDTO(File file);

    File fileDTOToFile(FileDTO fileDTO);
}
