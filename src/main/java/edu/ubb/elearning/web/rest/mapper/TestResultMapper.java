package edu.ubb.elearning.web.rest.mapper;

import edu.ubb.elearning.domain.*;
import edu.ubb.elearning.web.rest.dto.TestResultDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity TestResult and its DTO TestResultDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface TestResultMapper {

    @Mapping(source = "user.id", target = "userId")
    @Mapping(source = "user.login", target = "userLogin")
    @Mapping(source = "tests.id", target = "testsId")
    @Mapping(source = "tests.name", target = "testsName")
    TestResultDTO testResultToTestResultDTO(TestResult testResult);

    @Mapping(source = "userId", target = "user")
    @Mapping(source = "testsId", target = "tests")
    TestResult testResultDTOToTestResult(TestResultDTO testResultDTO);

    default User userFromId(Long id) {
        if (id == null) {
            return null;
        }
        User user = new User();
        user.setId(id);
        return user;
    }

    default Tests testsFromId(Long id) {
        if (id == null) {
            return null;
        }
        Tests tests = new Tests();
        tests.setId(id);
        return tests;
    }
}
