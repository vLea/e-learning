package edu.ubb.elearning.web.rest.mapper;

import edu.ubb.elearning.domain.*;
import edu.ubb.elearning.web.rest.dto.CourseDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Course and its DTO CourseDTO.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class, })
public interface CourseMapper {

    @Mapping(source = "teacher.id", target = "teacherId")
    CourseDTO courseToCourseDTO(Course course);

    @Mapping(source = "teacherId", target = "teacher")
    Course courseDTOToCourse(CourseDTO courseDTO);

    default Teacher teacherFromId(Long id) {
        if (id == null) {
            return null;
        }
        Teacher teacher = new Teacher();
        teacher.setId(id);
        return teacher;
    }

    default User userFromId(Long id) {
        if (id == null) {
            return null;
        }
        User user = new User();
        user.setId(id);
        return user;
    }
}
