package edu.ubb.elearning.web.rest.mapper;

import edu.ubb.elearning.domain.*;
import edu.ubb.elearning.web.rest.dto.DiscussionDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Discussion and its DTO DiscussionDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface DiscussionMapper {

    @Mapping(source = "groups.id", target = "groupsId")
    @Mapping(source = "groups.name", target = "groupsName")
    DiscussionDTO discussionToDiscussionDTO(Discussion discussion);

    @Mapping(source = "groupsId", target = "groups")
    Discussion discussionDTOToDiscussion(DiscussionDTO discussionDTO);

    default Groups groupsFromId(Long id) {
        if (id == null) {
            return null;
        }
        Groups groups = new Groups();
        groups.setId(id);
        return groups;
    }
}
