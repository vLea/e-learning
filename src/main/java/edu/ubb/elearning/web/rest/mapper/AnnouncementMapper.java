package edu.ubb.elearning.web.rest.mapper;

import edu.ubb.elearning.domain.*;
import edu.ubb.elearning.web.rest.dto.AnnouncementDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Announcement and its DTO AnnouncementDTO.
 */
@Mapper(componentModel = "spring", uses = {FileMapper.class, })
public interface AnnouncementMapper {

    @Mapping(source = "groups.id", target = "groupsId")
    @Mapping(source = "groups.name", target = "groupsName")
    AnnouncementDTO announcementToAnnouncementDTO(Announcement announcement);

    @Mapping(source = "groupsId", target = "groups")
    Announcement announcementDTOToAnnouncement(AnnouncementDTO announcementDTO);

    default File fileFromId(Long id) {
        if (id == null) {
            return null;
        }
        File file = new File();
        file.setId(id);
        return file;
    }

    default Groups groupsFromId(Long id) {
        if (id == null) {
            return null;
        }
        Groups groups = new Groups();
        groups.setId(id);
        return groups;
    }
}
