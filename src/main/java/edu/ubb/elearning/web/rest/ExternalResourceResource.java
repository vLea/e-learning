package edu.ubb.elearning.web.rest;

import com.codahale.metrics.annotation.Timed;
import edu.ubb.elearning.domain.ExternalResource;
import edu.ubb.elearning.service.ExternalResourceService;
import edu.ubb.elearning.web.rest.util.HeaderUtil;
import edu.ubb.elearning.web.rest.util.PaginationUtil;
import edu.ubb.elearning.web.rest.dto.ExternalResourceDTO;
import edu.ubb.elearning.web.rest.mapper.ExternalResourceMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * REST controller for managing ExternalResource.
 */
@RestController
@RequestMapping("/api")
public class ExternalResourceResource {

    private final Logger log = LoggerFactory.getLogger(ExternalResourceResource.class);

    @Inject
    private ExternalResourceService externalResourceService;

    @Inject
    private ExternalResourceMapper externalResourceMapper;

    /**
     * POST  /externalResources -> Create a new externalResource.
     */
    @RequestMapping(value = "/externalResources",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ExternalResourceDTO> createExternalResource(@RequestBody ExternalResourceDTO externalResourceDTO) throws URISyntaxException {
        log.debug("REST request to save ExternalResource : {}", externalResourceDTO);
        if (externalResourceDTO.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new externalResource cannot already have an ID").body(null);
        }
        ExternalResource externalResource = externalResourceMapper.externalResourceDTOToExternalResource(externalResourceDTO);
        ExternalResource result = externalResourceService.createExternalResource(externalResource);
        return ResponseEntity.created(new URI("/api/externalResources/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("externalResource", result.getId().toString()))
            .body(externalResourceMapper.externalResourceToExternalResourceDTO(result));
    }

    /**
     * PUT  /externalResources -> Updates an existing externalResource.
     */
    @RequestMapping(value = "/externalResources",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ExternalResourceDTO> updateExternalResource(@RequestBody ExternalResourceDTO externalResourceDTO) throws URISyntaxException {
        log.debug("REST request to update ExternalResource : {}", externalResourceDTO);
        if (externalResourceDTO.getId() == null) {
            return createExternalResource(externalResourceDTO);
        }
        ExternalResource externalResource = externalResourceMapper.externalResourceDTOToExternalResource(externalResourceDTO);
        ExternalResource result = externalResourceService.updateExternalResource(externalResource);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("externalResource", externalResourceDTO.getId().toString()))
            .body(externalResourceMapper.externalResourceToExternalResourceDTO(result));
    }

    /**
     * GET  /externalResources -> get all the externalResources.
     */
    @RequestMapping(value = "/externalResources",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<ExternalResourceDTO>> getAllExternalResources(Pageable pageable)
        throws URISyntaxException {
        Page<ExternalResource> page = externalResourceService.getAllExternalResources(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/externalResources");
        return new ResponseEntity<>(page.getContent().stream()
            .map(externalResourceMapper::externalResourceToExternalResourceDTO)
            .collect(Collectors.toCollection(LinkedList::new)), headers, HttpStatus.OK);
    }

    /**
     * GET  /externalResources/:id -> get the "id" externalResource.
     */
    @RequestMapping(value = "/externalResources/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ExternalResourceDTO> getExternalResource(@PathVariable Long id) {
        log.debug("REST request to get ExternalResource : {}", id);
        return Optional.ofNullable(externalResourceService.getExternalResource(id))
            .map(externalResourceMapper::externalResourceToExternalResourceDTO)
            .map(externalResourceDTO -> new ResponseEntity<>(
                externalResourceDTO,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /externalResources/:id -> delete the "id" externalResource.
     */
    @RequestMapping(value = "/externalResources/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteExternalResource(@PathVariable Long id) {
        log.debug("REST request to delete ExternalResource : {}", id);
        externalResourceService.deleteExternalResource(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("externalResource", id.toString())).build();
    }
}
