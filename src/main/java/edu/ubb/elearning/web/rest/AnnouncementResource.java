package edu.ubb.elearning.web.rest;

import com.codahale.metrics.annotation.Timed;
import edu.ubb.elearning.domain.Announcement;
import edu.ubb.elearning.service.AnnouncementService;
import edu.ubb.elearning.web.rest.util.HeaderUtil;
import edu.ubb.elearning.web.rest.util.PaginationUtil;
import edu.ubb.elearning.web.rest.dto.AnnouncementDTO;
import edu.ubb.elearning.web.rest.mapper.AnnouncementMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * REST controller for managing Announcement.
 */
@RestController
@RequestMapping("/api")
public class AnnouncementResource {

    private final Logger log = LoggerFactory.getLogger(AnnouncementResource.class);
    
    @Inject
    private AnnouncementService announcementService;
    
    @Inject
    private AnnouncementMapper announcementMapper;

    /**
     * POST  /announcements -> Create a new announcement.
     */
    @RequestMapping(value = "/announcements",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<AnnouncementDTO> createAnnouncement(@Valid @RequestBody AnnouncementDTO announcementDTO) throws URISyntaxException {
        log.debug("REST request to save Announcement : {}", announcementDTO);
        if (announcementDTO.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new announcement cannot already have an ID").body(null);
        }
        Announcement announcement = announcementMapper.announcementDTOToAnnouncement(announcementDTO);
        Announcement result = announcementService.createAnnouncement(announcement);
        return ResponseEntity.created(new URI("/api/announcements/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("announcement", result.getId().toString()))
            .body(announcementMapper.announcementToAnnouncementDTO(result));
    }

    /**
     * PUT  /announcements -> Updates an existing announcement.
     */
    @RequestMapping(value = "/announcements",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<AnnouncementDTO> updateAnnouncement(@Valid @RequestBody AnnouncementDTO announcementDTO) throws URISyntaxException {
        log.debug("REST request to update Announcement : {}", announcementDTO);
        if (announcementDTO.getId() == null) {
            return createAnnouncement(announcementDTO);
        }
        Announcement announcement = announcementMapper.announcementDTOToAnnouncement(announcementDTO);
        Announcement result = announcementService.updateAnnouncement(announcement);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("announcement", announcementDTO.getId().toString()))
            .body(announcementMapper.announcementToAnnouncementDTO(result));
    }

    /**
     * GET  /announcements -> get all the announcements.
     */
    @RequestMapping(value = "/announcements",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<AnnouncementDTO>> getAllAnnouncements(Pageable pageable)
        throws URISyntaxException {
        Page<Announcement> page = announcementService.getAllAnnouncements(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/announcements");
        return new ResponseEntity<>(page.getContent().stream()
            .map(announcementMapper::announcementToAnnouncementDTO)
            .collect(Collectors.toCollection(LinkedList::new)), headers, HttpStatus.OK);
    }

    /**
     * GET  /announcements/:id -> get the "id" announcement.
     */
    @RequestMapping(value = "/announcements/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<AnnouncementDTO> getAnnouncement(@PathVariable Long id) {
        log.debug("REST request to get Announcement : {}", id);
        return Optional.ofNullable(announcementService.getAnnouncement(id))
            .map(announcementMapper::announcementToAnnouncementDTO)
            .map(announcementDTO -> new ResponseEntity<>(
                announcementDTO,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /announcements/:id -> delete the "id" announcement.
     */
    @RequestMapping(value = "/announcements/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteAnnouncement(@PathVariable Long id) {
        log.debug("REST request to delete Announcement : {}", id);
        announcementService.deleteAnnouncement(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("announcement", id.toString())).build();
    }
}
