package edu.ubb.elearning.web.rest.mapper;

import org.mapstruct.Mapper;

import edu.ubb.elearning.domain.User;
import edu.ubb.elearning.web.rest.dto.UserDTO;

@Mapper(componentModel = "spring", uses = {AuthorityMapper.class })
public interface UserMapper {
	
	UserDTO userToUserDTO(User user);

    User userDTOToUser(UserDTO userDTO);
 
}
