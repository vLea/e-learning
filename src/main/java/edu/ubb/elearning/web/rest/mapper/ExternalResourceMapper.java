package edu.ubb.elearning.web.rest.mapper;

import edu.ubb.elearning.domain.*;
import edu.ubb.elearning.web.rest.dto.ExternalResourceDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity ExternalResource and its DTO ExternalResourceDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ExternalResourceMapper {

    ExternalResourceDTO externalResourceToExternalResourceDTO(ExternalResource externalResource);

    ExternalResource externalResourceDTOToExternalResource(ExternalResourceDTO externalResourceDTO);
}
