package edu.ubb.elearning.web.rest;

import com.codahale.metrics.annotation.Timed;
import edu.ubb.elearning.domain.TestResult;
import edu.ubb.elearning.service.TestResultService;
import edu.ubb.elearning.web.rest.util.HeaderUtil;
import edu.ubb.elearning.web.rest.util.PaginationUtil;
import edu.ubb.elearning.web.rest.dto.TestResultDTO;
import edu.ubb.elearning.web.rest.mapper.TestResultMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * REST controller for managing TestResult.
 */
@RestController
@RequestMapping("/api")
public class TestResultResource {

    private final Logger log = LoggerFactory.getLogger(TestResultResource.class);

    @Inject
    private TestResultService testResultService;

    @Inject
    private TestResultMapper testResultMapper;

    /**
     * POST  /testResults -> Create a new testResult.
     */
    @RequestMapping(value = "/testResults",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TestResultDTO> createTestResult(@Valid @RequestBody TestResultDTO testResultDTO) throws URISyntaxException {
        log.debug("REST request to save TestResult : {}", testResultDTO);
        if (testResultDTO.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new testResult cannot already have an ID").body(null);
        }
        TestResult testResult = testResultMapper.testResultDTOToTestResult(testResultDTO);
        TestResult result = testResultService.createTestResult(testResult);
        return ResponseEntity.created(new URI("/api/testResults/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("testResult", result.getId().toString()))
            .body(testResultMapper.testResultToTestResultDTO(result));
    }

    /**
     * PUT  /testResults -> Updates an existing testResult.
     */
    @RequestMapping(value = "/testResults",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TestResultDTO> updateTestResult(@Valid @RequestBody TestResultDTO testResultDTO) throws URISyntaxException {
        log.debug("REST request to update TestResult : {}", testResultDTO);
        if (testResultDTO.getId() == null) {
            return createTestResult(testResultDTO);
        }
        TestResult testResult = testResultMapper.testResultDTOToTestResult(testResultDTO);
        TestResult result = testResultService.updateTestResult(testResult);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("testResult", testResultDTO.getId().toString()))
            .body(testResultMapper.testResultToTestResultDTO(result));
    }

    /**
     * GET  /testResults -> get all the testResults.
     */
    @RequestMapping(value = "/testResults",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<TestResultDTO>> getAllTestResults(Pageable pageable)
        throws URISyntaxException {
        Page<TestResult> page = testResultService.getAllTestResults(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/testResults");
        return new ResponseEntity<>(page.getContent().stream()
            .map(testResultMapper::testResultToTestResultDTO)
            .collect(Collectors.toCollection(LinkedList::new)), headers, HttpStatus.OK);
    }

    /**
     * GET  /testResults/:id -> get the "id" testResult.
     */
    @RequestMapping(value = "/testResults/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TestResultDTO> getTestResult(@PathVariable Long id) {
        log.debug("REST request to get TestResult : {}", id);
        return Optional.ofNullable(testResultService.getTestResult(id))
            .map(testResultMapper::testResultToTestResultDTO)
            .map(testResultDTO -> new ResponseEntity<>(
                testResultDTO,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /testResults/:id -> delete the "id" testResult.
     */
    @RequestMapping(value = "/testResults/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteTestResult(@PathVariable Long id) {
        log.debug("REST request to delete TestResult : {}", id);
        testResultService.deleteTestResult(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("testResult", id.toString())).build();
    }
}
