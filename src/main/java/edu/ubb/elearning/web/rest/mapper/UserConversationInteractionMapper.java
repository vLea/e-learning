package edu.ubb.elearning.web.rest.mapper;

import edu.ubb.elearning.domain.*;
import edu.ubb.elearning.web.rest.dto.UserConversationInteractionDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity UserConversationInteraction and its DTO UserConversationInteractionDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface UserConversationInteractionMapper {

    @Mapping(source = "user.id", target = "userId")
    @Mapping(source = "user.login", target = "userLogin")
    @Mapping(source = "conversation.id", target = "conversationId")
    @Mapping(source = "conversation.name", target = "conversationName")
    UserConversationInteractionDTO userConversationInteractionToUserConversationInteractionDTO(UserConversationInteraction userConversationInteraction);

    @Mapping(source = "userId", target = "user")
    @Mapping(source = "conversationId", target = "conversation")
    UserConversationInteraction userConversationInteractionDTOToUserConversationInteraction(UserConversationInteractionDTO userConversationInteractionDTO);

    default User userFromId(Long id) {
        if (id == null) {
            return null;
        }
        User user = new User();
        user.setId(id);
        return user;
    }

    default Conversation conversationFromId(Long id) {
        if (id == null) {
            return null;
        }
        Conversation conversation = new Conversation();
        conversation.setId(id);
        return conversation;
    }
}
