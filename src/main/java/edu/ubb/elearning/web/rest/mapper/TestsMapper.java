package edu.ubb.elearning.web.rest.mapper;

import edu.ubb.elearning.domain.*;
import edu.ubb.elearning.web.rest.dto.TestsDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Tests and its DTO TestsDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface TestsMapper {

    @Mapping(source = "course.id", target = "courseId")
    @Mapping(source = "course.name", target = "courseName")
    @Mapping(source = "groups.id", target = "groupsId")
    @Mapping(source = "groups.name", target = "groupsName")
    TestsDTO testsToTestsDTO(Tests tests);

    @Mapping(source = "courseId", target = "course")
    @Mapping(source = "groupsId", target = "groups")
    Tests testsDTOToTests(TestsDTO testsDTO);

    default Course courseFromId(Long id) {
        if (id == null) {
            return null;
        }
        Course course = new Course();
        course.setId(id);
        return course;
    }

    default Groups groupsFromId(Long id) {
        if (id == null) {
            return null;
        }
        Groups groups = new Groups();
        groups.setId(id);
        return groups;
    }
}
