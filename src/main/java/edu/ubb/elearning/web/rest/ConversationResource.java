package edu.ubb.elearning.web.rest;

import com.codahale.metrics.annotation.Timed;
import edu.ubb.elearning.domain.Conversation;
import edu.ubb.elearning.service.ConversationService;
import edu.ubb.elearning.web.rest.util.HeaderUtil;
import edu.ubb.elearning.web.rest.util.PaginationUtil;
import edu.ubb.elearning.web.rest.dto.ConversationDTO;
import edu.ubb.elearning.web.rest.mapper.ConversationMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * REST controller for managing Conversation.
 */
@RestController
@RequestMapping("/api")
public class ConversationResource {

    private final Logger log = LoggerFactory.getLogger(ConversationResource.class);

    @Inject
    private ConversationService conversationService;

    @Inject
    private ConversationMapper conversationMapper;

    /**
     * POST  /conversations -> Create a new conversation.
     */
    @RequestMapping(value = "/conversations",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ConversationDTO> createConversation(@RequestBody ConversationDTO conversationDTO) throws URISyntaxException {
        log.debug("REST request to save Conversation : {}", conversationDTO);
        if (conversationDTO.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new conversation cannot already have an ID").body(null);
        }
        Conversation conversation = conversationMapper.conversationDTOToConversation(conversationDTO);
        Conversation result = conversationService.createConversation(conversation);
        return ResponseEntity.created(new URI("/api/conversations/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("conversation", result.getId().toString()))
            .body(conversationMapper.conversationToConversationDTO(result));
    }

    /**
     * PUT  /conversations -> Updates an existing conversation.
     */
    @RequestMapping(value = "/conversations",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ConversationDTO> updateConversation(@RequestBody ConversationDTO conversationDTO) throws URISyntaxException {
        log.debug("REST request to update Conversation : {}", conversationDTO);
        if (conversationDTO.getId() == null) {
            return createConversation(conversationDTO);
        }
        Conversation conversation = conversationMapper.conversationDTOToConversation(conversationDTO);
        Conversation result = conversationService.updateConversation(conversation);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("conversation", conversationDTO.getId().toString()))
            .body(conversationMapper.conversationToConversationDTO(result));
    }

    /**
     * GET  /conversations -> get all the conversations.
     */
    @RequestMapping(value = "/conversations",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<ConversationDTO>> getAllConversations(Pageable pageable)
        throws URISyntaxException {
        Page<Conversation> page = conversationService.getAllConversations(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/conversations");
        return new ResponseEntity<>(page.getContent().stream()
            .map(conversationMapper::conversationToConversationDTO)
            .collect(Collectors.toCollection(LinkedList::new)), headers, HttpStatus.OK);
    }

    /**
     * GET  /conversations/:id -> get the "id" conversation.
     */
    @RequestMapping(value = "/conversations/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ConversationDTO> getConversation(@PathVariable Long id) {
        log.debug("REST request to get Conversation : {}", id);
        return Optional.ofNullable(conversationService.getConversation(id))
            .map(conversationMapper::conversationToConversationDTO)
            .map(conversationDTO -> new ResponseEntity<>(
                conversationDTO,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /conversations/:id -> delete the "id" conversation.
     */
    @RequestMapping(value = "/conversations/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteConversation(@PathVariable Long id) {
        log.debug("REST request to delete Conversation : {}", id);
        conversationService.deleteConversation(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("conversation", id.toString())).build();
    }
}
