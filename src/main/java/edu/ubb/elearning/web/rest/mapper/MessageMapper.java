package edu.ubb.elearning.web.rest.mapper;

import edu.ubb.elearning.domain.*;
import edu.ubb.elearning.web.rest.dto.MessageDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Message and its DTO MessageDTO.
 */
@Mapper(componentModel = "spring", uses = {FileMapper.class, })
public interface MessageMapper {

    @Mapping(source = "conversation.id", target = "conversationId")
    @Mapping(source = "conversation.name", target = "conversationName")
    @Mapping(source = "userFrom.id", target = "userFromId")
    @Mapping(source = "userFrom.login", target = "userFromLogin")
    @Mapping(source = "userTo.id", target = "userToId")
    @Mapping(source = "userTo.login", target = "userToLogin")
    MessageDTO messageToMessageDTO(Message message);

    @Mapping(source = "conversationId", target = "conversation")
    @Mapping(source = "userFromId", target = "userFrom")
    @Mapping(source = "userToId", target = "userTo")
    Message messageDTOToMessage(MessageDTO messageDTO);

    default Conversation conversationFromId(Long id) {
        if (id == null) {
            return null;
        }
        Conversation conversation = new Conversation();
        conversation.setId(id);
        return conversation;
    }

    default User userFromId(Long id) {
        if (id == null) {
            return null;
        }
        User user = new User();
        user.setId(id);
        return user;
    }

    default File fileFromId(Long id) {
        if (id == null) {
            return null;
        }
        File file = new File();
        file.setId(id);
        return file;
    }
}
