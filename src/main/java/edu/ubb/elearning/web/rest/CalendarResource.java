package edu.ubb.elearning.web.rest;

import com.codahale.metrics.annotation.Timed;
import edu.ubb.elearning.domain.Calendar;
import edu.ubb.elearning.service.CalendarService;
import edu.ubb.elearning.web.rest.util.HeaderUtil;
import edu.ubb.elearning.web.rest.util.PaginationUtil;
import edu.ubb.elearning.web.rest.dto.CalendarDTO;
import edu.ubb.elearning.web.rest.mapper.CalendarMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * REST controller for managing Calendar.
 */
@RestController
@RequestMapping("/api")
public class CalendarResource {

    private final Logger log = LoggerFactory.getLogger(CalendarResource.class);

    @Inject
    private CalendarService calendarService;

    @Inject
    private CalendarMapper calendarMapper;

    /**
     * POST  /calendars -> Create a new calendar.
     */
    @RequestMapping(value = "/calendars",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<CalendarDTO> createCalendar(@RequestBody CalendarDTO calendarDTO) throws URISyntaxException {
        log.debug("REST request to save Calendar : {}", calendarDTO);
        if (calendarDTO.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new calendar cannot already have an ID").body(null);
        }
        Calendar calendar = calendarMapper.calendarDTOToCalendar(calendarDTO);
        Calendar result = calendarService.createCalendar(calendar);
        return ResponseEntity.created(new URI("/api/calendars/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("calendar", result.getId().toString()))
            .body(calendarMapper.calendarToCalendarDTO(result));
    }

    /**
     * PUT  /calendars -> Updates an existing calendar.
     */
    @RequestMapping(value = "/calendars",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<CalendarDTO> updateCalendar(@RequestBody CalendarDTO calendarDTO) throws URISyntaxException {
        log.debug("REST request to update Calendar : {}", calendarDTO);
        Calendar calendar = calendarMapper.calendarDTOToCalendar(calendarDTO);
        Calendar result = calendarService.updateCalendar(calendar);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("calendar", calendarDTO.getId().toString()))
            .body(calendarMapper.calendarToCalendarDTO(result));
    }

    /**
     * GET  /calendars -> get all the calendars.
     */
    @RequestMapping(value = "/calendars",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<CalendarDTO>> getAllCalendars(Pageable pageable)
        throws URISyntaxException {
        Page<Calendar> page = calendarService.getAllCalendars(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/calendars");
        return new ResponseEntity<>(page.getContent().stream()
            .map(calendarMapper::calendarToCalendarDTO)
            .collect(Collectors.toCollection(LinkedList::new)), headers, HttpStatus.OK);
    }

    /**
     * GET  /calendars/:id -> get the "id" calendar.
     */
    @RequestMapping(value = "/calendars/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<CalendarDTO> getCalendar(@PathVariable Long id) {
        log.debug("REST request to get Calendar : {}", id);
        return Optional.ofNullable(calendarService.getCalendar(id))
            .map(calendarMapper::calendarToCalendarDTO)
            .map(calendarDTO -> new ResponseEntity<>(
                calendarDTO,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /calendars/:id -> delete the "id" calendar.
     */
    @RequestMapping(value = "/calendars/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteCalendar(@PathVariable Long id) {
        log.debug("REST request to delete Calendar : {}", id);
        calendarService.deleteCalendar(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("calendar", id.toString())).build();
    }
}
