package edu.ubb.elearning.web.rest.mapper;

import edu.ubb.elearning.domain.*;
import edu.ubb.elearning.web.rest.dto.CalendarDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Calendar and its DTO CalendarDTO.
 */
@Mapper(componentModel = "spring", uses = {EventMapper.class, })
public interface CalendarMapper {

    @Mapping(source = "user.id", target = "userId")
    @Mapping(source = "user.login", target = "userLogin")
    CalendarDTO calendarToCalendarDTO(Calendar calendar);

    @Mapping(source = "userId", target = "user")
    Calendar calendarDTOToCalendar(CalendarDTO calendarDTO);

    default User userFromId(Long id) {
        if (id == null) {
            return null;
        }
        User user = new User();
        user.setId(id);
        return user;
    }

    default Event eventFromId(Long id) {
        if (id == null) {
            return null;
        }
        Event event = new Event();
        event.setId(id);
        return event;
    }
}
