package edu.ubb.elearning.web.rest.mapper;

import edu.ubb.elearning.domain.*;
import edu.ubb.elearning.web.rest.dto.QuestionDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Question and its DTO QuestionDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface QuestionMapper {

    @Mapping(source = "tests.id", target = "testsId")
    @Mapping(source = "tests.name", target = "testsName")
    QuestionDTO questionToQuestionDTO(Question question);

    @Mapping(source = "testsId", target = "tests")
    Question questionDTOToQuestion(QuestionDTO questionDTO);

    default Tests testsFromId(Long id) {
        if (id == null) {
            return null;
        }
        Tests tests = new Tests();
        tests.setId(id);
        return tests;
    }
}
