package edu.ubb.elearning.web.rest;

import com.codahale.metrics.annotation.Timed;
import edu.ubb.elearning.domain.Groups;
import edu.ubb.elearning.service.GroupsService;
import edu.ubb.elearning.web.rest.util.HeaderUtil;
import edu.ubb.elearning.web.rest.util.PaginationUtil;
import edu.ubb.elearning.web.rest.dto.GroupsDTO;
import edu.ubb.elearning.web.rest.mapper.GroupsMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * REST controller for managing Groups.
 */
@RestController
@RequestMapping("/api")
public class GroupsResource {

    private final Logger log = LoggerFactory.getLogger(GroupsResource.class);

    @Inject
    private GroupsService groupsService;

    @Inject
    private GroupsMapper groupsMapper;

    /**
     * POST  /groupss -> Create a new groups.
     */
    @RequestMapping(value = "/groupss",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<GroupsDTO> createGroups(@RequestBody GroupsDTO groupsDTO) throws URISyntaxException {
        log.debug("REST request to save Groups : {}", groupsDTO);
        if (groupsDTO.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new groups cannot already have an ID").body(null);
        }
        Groups groups = groupsMapper.groupsDTOToGroups(groupsDTO);
        Groups result = groupsService.createGroups(groups);
        return ResponseEntity.created(new URI("/api/groupss/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("groups", result.getId().toString()))
            .body(groupsMapper.groupsToGroupsDTO(result));
    }

    /**
     * PUT  /groupss -> Updates an existing groups.
     */
    @RequestMapping(value = "/groupss",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<GroupsDTO> updateGroups(@RequestBody GroupsDTO groupsDTO) throws URISyntaxException {
        log.debug("REST request to update Groups : {}", groupsDTO);
        if (groupsDTO.getId() == null) {
            return createGroups(groupsDTO);
        }
        Groups groups = groupsMapper.groupsDTOToGroups(groupsDTO);
        Groups result = groupsService.updateGroups(groups);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("groups", groupsDTO.getId().toString()))
            .body(groupsMapper.groupsToGroupsDTO(result));
    }

    /**
     * GET  /groupss -> get all the groupss.
     */
    @RequestMapping(value = "/groupss",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<GroupsDTO>> getAllGroupss(Pageable pageable)
        throws URISyntaxException {
        Page<Groups> page = groupsService.getAllGroupss(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/groupss");
        return new ResponseEntity<>(page.getContent().stream()
            .map(groupsMapper::groupsToGroupsDTO)
            .collect(Collectors.toCollection(LinkedList::new)), headers, HttpStatus.OK);
    }

    /**
     * GET  /groupss/:id -> get the "id" groups.
     */
    @RequestMapping(value = "/groupss/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<GroupsDTO> getGroups(@PathVariable Long id) {
        log.debug("REST request to get Groups : {}", id);
        return Optional.ofNullable(groupsService.getGroups(id))
            .map(groupsMapper::groupsToGroupsDTO)
            .map(groupsDTO -> new ResponseEntity<>(
                groupsDTO,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /groupss/:id -> delete the "id" groups.
     */
    @RequestMapping(value = "/groupss/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteGroups(@PathVariable Long id) {
        log.debug("REST request to delete Groups : {}", id);
        groupsService.deleteGroups(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("groups", id.toString())).build();
    }
}
