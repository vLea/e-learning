package edu.ubb.elearning.web.rest;

import com.codahale.metrics.annotation.Timed;
import edu.ubb.elearning.domain.ConversationComment;
import edu.ubb.elearning.service.ConversationCommentService;
import edu.ubb.elearning.web.rest.util.HeaderUtil;
import edu.ubb.elearning.web.rest.util.PaginationUtil;
import edu.ubb.elearning.web.rest.dto.ConversationCommentDTO;
import edu.ubb.elearning.web.rest.mapper.ConversationCommentMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * REST controller for managing ConversationComment.
 */
@RestController
@RequestMapping("/api")
public class ConversationCommentResource {

    private final Logger log = LoggerFactory.getLogger(ConversationCommentResource.class);

    @Inject
    private ConversationCommentService conversationCommentService;

    @Inject
    private ConversationCommentMapper conversationCommentMapper;

    /**
     * POST  /conversationComments -> Create a new conversationComment.
     */
    @RequestMapping(value = "/conversationComments",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ConversationCommentDTO> createConversationComment(@RequestBody ConversationCommentDTO conversationCommentDTO) throws URISyntaxException {
        log.debug("REST request to save ConversationComment : {}", conversationCommentDTO);
        if (conversationCommentDTO.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new conversationComment cannot already have an ID").body(null);
        }
        ConversationComment conversationComment = conversationCommentMapper.conversationCommentDTOToConversationComment(conversationCommentDTO);
        ConversationComment result = conversationCommentService.createConversationComment(conversationComment);
        return ResponseEntity.created(new URI("/api/conversationComments/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("conversationComment", result.getId().toString()))
            .body(conversationCommentMapper.conversationCommentToConversationCommentDTO(result));
    }

    /**
     * PUT  /conversationComments -> Updates an existing conversationComment.
     */
    @RequestMapping(value = "/conversationComments",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ConversationCommentDTO> updateConversationComment(@RequestBody ConversationCommentDTO conversationCommentDTO) throws URISyntaxException {
        log.debug("REST request to update ConversationComment : {}", conversationCommentDTO);
        if (conversationCommentDTO.getId() == null) {
            return createConversationComment(conversationCommentDTO);
        }
        ConversationComment conversationComment = conversationCommentMapper.conversationCommentDTOToConversationComment(conversationCommentDTO);
        ConversationComment result = conversationCommentService.updateConversationComment(conversationComment);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("conversationComment", conversationCommentDTO.getId().toString()))
            .body(conversationCommentMapper.conversationCommentToConversationCommentDTO(result));
    }

    /**
     * GET  /conversationComments -> get all the conversationComments.
     */
    @RequestMapping(value = "/conversationComments",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<ConversationCommentDTO>> getAllConversationComments(Pageable pageable)
        throws URISyntaxException {
        Page<ConversationComment> page = conversationCommentService.getAllConversationComments(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/conversationComments");
        return new ResponseEntity<>(page.getContent().stream()
            .map(conversationCommentMapper::conversationCommentToConversationCommentDTO)
            .collect(Collectors.toCollection(LinkedList::new)), headers, HttpStatus.OK);
    }

    /**
     * GET  /conversationComments/:id -> get the "id" conversationComment.
     */
    @RequestMapping(value = "/conversationComments/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ConversationCommentDTO> getConversationComment(@PathVariable Long id) {
        log.debug("REST request to get ConversationComment : {}", id);
        return Optional.ofNullable(conversationCommentService.getConversationComment(id))
            .map(conversationCommentMapper::conversationCommentToConversationCommentDTO)
            .map(conversationCommentDTO -> new ResponseEntity<>(
                conversationCommentDTO,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /conversationComments/:id -> delete the "id" conversationComment.
     */
    @RequestMapping(value = "/conversationComments/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteConversationComment(@PathVariable Long id) {
        log.debug("REST request to delete ConversationComment : {}", id);
        conversationCommentService.deleteConversationComment(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("conversationComment", id.toString())).build();
    }
}
