package edu.ubb.elearning.web.rest;

import com.codahale.metrics.annotation.Timed;
import edu.ubb.elearning.domain.Tests;
import edu.ubb.elearning.service.TestsService;
import edu.ubb.elearning.web.rest.util.HeaderUtil;
import edu.ubb.elearning.web.rest.util.PaginationUtil;
import edu.ubb.elearning.web.rest.dto.TestsDTO;
import edu.ubb.elearning.web.rest.mapper.TestsMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * REST controller for managing Tests.
 */
@RestController
@RequestMapping("/api")
public class TestsResource {

    private final Logger log = LoggerFactory.getLogger(TestsResource.class);

    @Inject
    private TestsService testsService;

    @Inject
    private TestsMapper testsMapper;

    /**
     * POST  /testss -> Create a new tests.
     */
    @RequestMapping(value = "/testss",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TestsDTO> createTests(@RequestBody TestsDTO testsDTO) throws URISyntaxException {
        log.debug("REST request to save Tests : {}", testsDTO);
        if (testsDTO.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new tests cannot already have an ID").body(null);
        }
        Tests tests = testsMapper.testsDTOToTests(testsDTO);
        Tests result = testsService.createTests(tests);
        return ResponseEntity.created(new URI("/api/testss/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("tests", result.getId().toString()))
            .body(testsMapper.testsToTestsDTO(result));
    }

    /**
     * PUT  /testss -> Updates an existing tests.
     */
    @RequestMapping(value = "/testss",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TestsDTO> updateTests(@RequestBody TestsDTO testsDTO) throws URISyntaxException {
        log.debug("REST request to update Tests : {}", testsDTO);
        if (testsDTO.getId() == null) {
            return createTests(testsDTO);
        }
        Tests tests = testsMapper.testsDTOToTests(testsDTO);
        Tests result = testsService.updateTests(tests);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("tests", testsDTO.getId().toString()))
            .body(testsMapper.testsToTestsDTO(result));
    }

    /**
     * GET  /testss -> get all the testss.
     */
    @RequestMapping(value = "/testss",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<TestsDTO>> getAllTestss(Pageable pageable)
        throws URISyntaxException {
        Page<Tests> page = testsService.getAllTestss(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/testss");
        return new ResponseEntity<>(page.getContent().stream()
            .map(testsMapper::testsToTestsDTO)
            .collect(Collectors.toCollection(LinkedList::new)), headers, HttpStatus.OK);
    }

    /**
     * GET  /testss/:id -> get the "id" tests.
     */
    @RequestMapping(value = "/testss/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TestsDTO> getTests(@PathVariable Long id) {
        log.debug("REST request to get Tests : {}", id);
        return Optional.ofNullable(testsService.getTests(id))
            .map(testsMapper::testsToTestsDTO)
            .map(testsDTO -> new ResponseEntity<>(
                testsDTO,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /testss/:id -> delete the "id" tests.
     */
    @RequestMapping(value = "/testss/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteTests(@PathVariable Long id) {
        log.debug("REST request to delete Tests : {}", id);
        testsService.deleteTests(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("tests", id.toString())).build();
    }
}
