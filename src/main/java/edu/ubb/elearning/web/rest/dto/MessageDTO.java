package edu.ubb.elearning.web.rest.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;


/**
 * A DTO for the Message entity.
 */
public class MessageDTO implements Serializable {

    private Long id;

    private String subject;

    private String text;

    private Long conversationId;

    private String conversationName;

    private Long userFromId;

    private String userFromLogin;

    private Long userToId;

    private String userToLogin;

    private Set<FileDTO> files = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Long getConversationId() {
        return conversationId;
    }

    public void setConversationId(Long conversationId) {
        this.conversationId = conversationId;
    }

    public String getConversationName() {
        return conversationName;
    }

    public void setConversationName(String conversationName) {
        this.conversationName = conversationName;
    }

    public Long getUserFromId() {
        return userFromId;
    }

    public void setUserFromId(Long userId) {
        this.userFromId = userId;
    }

    public String getUserFromLogin() {
        return userFromLogin;
    }

    public void setUserFromLogin(String userLogin) {
        this.userFromLogin = userLogin;
    }

    public Long getUserToId() {
        return userToId;
    }

    public void setUserToId(Long userId) {
        this.userToId = userId;
    }

    public String getUserToLogin() {
        return userToLogin;
    }

    public void setUserToLogin(String userLogin) {
        this.userToLogin = userLogin;
    }

    public Set<FileDTO> getFiles() {
        return files;
    }

    public void setFiles(Set<FileDTO> files) {
        this.files = files;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        MessageDTO messageDTO = (MessageDTO) o;

        if ( ! Objects.equals(id, messageDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "MessageDTO{" +
            "id=" + id +
            ", subject='" + subject + "'" +
            ", text='" + text + "'" +
            '}';
    }
}
