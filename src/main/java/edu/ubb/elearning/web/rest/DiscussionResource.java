package edu.ubb.elearning.web.rest;

import com.codahale.metrics.annotation.Timed;
import edu.ubb.elearning.domain.Discussion;
import edu.ubb.elearning.service.DiscussionService;
import edu.ubb.elearning.web.rest.util.HeaderUtil;
import edu.ubb.elearning.web.rest.util.PaginationUtil;
import edu.ubb.elearning.web.rest.dto.DiscussionDTO;
import edu.ubb.elearning.web.rest.mapper.DiscussionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * REST controller for managing Discussion.
 */
@RestController
@RequestMapping("/api")
public class DiscussionResource {

    private final Logger log = LoggerFactory.getLogger(DiscussionResource.class);

    @Inject
    private DiscussionService discussionService;

    @Inject
    private DiscussionMapper discussionMapper;

    /**
     * POST  /discussions -> Create a new discussion.
     */
    @RequestMapping(value = "/discussions",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<DiscussionDTO> createDiscussion(@Valid @RequestBody DiscussionDTO discussionDTO) throws URISyntaxException {
        log.debug("REST request to save Discussion : {}", discussionDTO);
        if (discussionDTO.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new discussion cannot already have an ID").body(null);
        }
        Discussion discussion = discussionMapper.discussionDTOToDiscussion(discussionDTO);
        Discussion result = discussionService.createDiscussion(discussion);
        return ResponseEntity.created(new URI("/api/discussions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("discussion", result.getId().toString()))
            .body(discussionMapper.discussionToDiscussionDTO(result));
    }

    /**
     * PUT  /discussions -> Updates an existing discussion.
     */
    @RequestMapping(value = "/discussions",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<DiscussionDTO> updateDiscussion(@Valid @RequestBody DiscussionDTO discussionDTO) throws URISyntaxException {
        log.debug("REST request to update Discussion : {}", discussionDTO);
        if (discussionDTO.getId() == null) {
            return createDiscussion(discussionDTO);
        }
        Discussion discussion = discussionMapper.discussionDTOToDiscussion(discussionDTO);
        Discussion result = discussionService.updateDiscussion(discussion);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("discussion", discussionDTO.getId().toString()))
            .body(discussionMapper.discussionToDiscussionDTO(result));
    }

    /**
     * GET  /discussions -> get all the discussions.
     */
    @RequestMapping(value = "/discussions",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<DiscussionDTO>> getAllDiscussions(Pageable pageable)
        throws URISyntaxException {
        Page<Discussion> page = discussionService.getAllDiscussions(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/discussions");
        return new ResponseEntity<>(page.getContent().stream()
            .map(discussionMapper::discussionToDiscussionDTO)
            .collect(Collectors.toCollection(LinkedList::new)), headers, HttpStatus.OK);
    }

    /**
     * GET  /discussions/:id -> get the "id" discussion.
     */
    @RequestMapping(value = "/discussions/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<DiscussionDTO> getDiscussion(@PathVariable Long id) {
        log.debug("REST request to get Discussion : {}", id);
        return Optional.ofNullable(discussionService.getDiscussion(id))
            .map(discussionMapper::discussionToDiscussionDTO)
            .map(discussionDTO -> new ResponseEntity<>(
                discussionDTO,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /discussions/:id -> delete the "id" discussion.
     */
    @RequestMapping(value = "/discussions/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteDiscussion(@PathVariable Long id) {
        log.debug("REST request to delete Discussion : {}", id);
        discussionService.deleteDiscussion(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("discussion", id.toString())).build();
    }
}
