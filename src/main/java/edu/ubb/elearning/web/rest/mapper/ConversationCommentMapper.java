package edu.ubb.elearning.web.rest.mapper;

import edu.ubb.elearning.domain.*;
import edu.ubb.elearning.web.rest.dto.ConversationCommentDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity ConversationComment and its DTO ConversationCommentDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ConversationCommentMapper {

    @Mapping(source = "user.id", target = "userId")
    @Mapping(source = "user.login", target = "userLogin")
    ConversationCommentDTO conversationCommentToConversationCommentDTO(ConversationComment conversationComment);

    @Mapping(source = "userId", target = "user")
    ConversationComment conversationCommentDTOToConversationComment(ConversationCommentDTO conversationCommentDTO);

    default User userFromId(Long id) {
        if (id == null) {
            return null;
        }
        User user = new User();
        user.setId(id);
        return user;
    }
}
