package edu.ubb.elearning.web.rest.mapper;

import edu.ubb.elearning.domain.*;
import edu.ubb.elearning.web.rest.dto.PagesDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Pages and its DTO PagesDTO.
 */
@Mapper(componentModel = "spring", uses = {FileMapper.class, })
public interface PagesMapper {

    @Mapping(source = "groups.id", target = "groupsId")
    @Mapping(source = "groups.name", target = "groupsName")
    PagesDTO pagesToPagesDTO(Pages pages);

    @Mapping(source = "groupsId", target = "groups")
    Pages pagesDTOToPages(PagesDTO pagesDTO);

    default Groups groupsFromId(Long id) {
        if (id == null) {
            return null;
        }
        Groups groups = new Groups();
        groups.setId(id);
        return groups;
    }

    default File fileFromId(Long id) {
        if (id == null) {
            return null;
        }
        File file = new File();
        file.setId(id);
        return file;
    }
}
