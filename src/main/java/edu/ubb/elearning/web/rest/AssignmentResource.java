package edu.ubb.elearning.web.rest;

import com.codahale.metrics.annotation.Timed;
import edu.ubb.elearning.domain.Assignment;
import edu.ubb.elearning.service.AssignmentService;
import edu.ubb.elearning.web.rest.util.HeaderUtil;
import edu.ubb.elearning.web.rest.util.PaginationUtil;
import edu.ubb.elearning.web.rest.dto.AssignmentDTO;
import edu.ubb.elearning.web.rest.mapper.AssignmentMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * REST controller for managing Assignment.
 */
@RestController
@RequestMapping("/api")
public class AssignmentResource {

    private final Logger log = LoggerFactory.getLogger(AssignmentResource.class);

    @Inject
    private AssignmentService assignmentService;
    
    @Inject
    private AssignmentMapper assignmentMapper;

    /**
     * POST  /assignments -> Create a new assignment.
     */
    @RequestMapping(value = "/assignments",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<AssignmentDTO> createAssignment(@Valid @RequestBody AssignmentDTO assignmentDTO) throws URISyntaxException {
        log.debug("REST request to save Assignment : {}", assignmentDTO);
        if (assignmentDTO.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new assignment cannot already have an ID").body(null);
        }
        Assignment assignment = assignmentMapper.assignmentDTOToAssignment(assignmentDTO);
        Assignment result = assignmentService.createAssignment(assignment);
        return ResponseEntity.created(new URI("/api/assignments/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("assignment", result.getId().toString()))
            .body(assignmentMapper.assignmentToAssignmentDTO(result));
    }

    /**
     * PUT  /assignments -> Updates an existing assignment.
     */
    @RequestMapping(value = "/assignments",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<AssignmentDTO> updateAssignment(@Valid @RequestBody AssignmentDTO assignmentDTO) throws URISyntaxException {
        log.debug("REST request to update Assignment : {}", assignmentDTO);
        if (assignmentDTO.getId() == null) {
            return createAssignment(assignmentDTO);
        }
        Assignment assignment = assignmentMapper.assignmentDTOToAssignment(assignmentDTO);
        Assignment result = assignmentService.updateAssignment(assignment);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("assignment", assignmentDTO.getId().toString()))
            .body(assignmentMapper.assignmentToAssignmentDTO(result));
    }

    /**
     * GET  /assignments -> get all the assignments.
     */
    @RequestMapping(value = "/assignments",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<AssignmentDTO>> getAllAssignments(Pageable pageable)
        throws URISyntaxException {
        Page<Assignment> page = assignmentService.getAllAssignments(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/assignments");
        return new ResponseEntity<>(page.getContent().stream()
            .map(assignmentMapper::assignmentToAssignmentDTO)
            .collect(Collectors.toCollection(LinkedList::new)), headers, HttpStatus.OK);
    }

    /**
     * GET  /assignments/:id -> get the "id" assignment.
     */
    @RequestMapping(value = "/assignments/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<AssignmentDTO> getAssignment(@PathVariable Long id) {
        log.debug("REST request to get Assignment : {}", id);
        return Optional.ofNullable(assignmentService.getAssignment(id))
            .map(assignmentMapper::assignmentToAssignmentDTO)
            .map(assignmentDTO -> new ResponseEntity<>(
                assignmentDTO,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /assignments/:id -> delete the "id" assignment.
     */
    @RequestMapping(value = "/assignments/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteAssignment(@PathVariable Long id) {
        log.debug("REST request to delete Assignment : {}", id);
        assignmentService.deleteAssignment(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("assignment", id.toString())).build();
    }
}
