/**
 * Data Transfer Objects used by Spring MVC REST controllers.
 */
package edu.ubb.elearning.web.rest.dto;
