package edu.ubb.elearning.web.rest;

import com.codahale.metrics.annotation.Timed;
import edu.ubb.elearning.domain.UserConversationInteraction;
import edu.ubb.elearning.service.UserConversationInteractionService;
import edu.ubb.elearning.web.rest.util.HeaderUtil;
import edu.ubb.elearning.web.rest.util.PaginationUtil;
import edu.ubb.elearning.web.rest.dto.UserConversationInteractionDTO;
import edu.ubb.elearning.web.rest.mapper.UserConversationInteractionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * REST controller for managing UserConversationInteraction.
 */
@RestController
@RequestMapping("/api")
public class UserConversationInteractionResource {

    private final Logger log = LoggerFactory.getLogger(UserConversationInteractionResource.class);

    @Inject
    private UserConversationInteractionService userConversationInteractionService;

    @Inject
    private UserConversationInteractionMapper userConversationInteractionMapper;

    /**
     * POST  /userConversationInteractions -> Create a new userConversationInteraction.
     */
    @RequestMapping(value = "/userConversationInteractions",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<UserConversationInteractionDTO> createUserConversationInteraction(@RequestBody UserConversationInteractionDTO userConversationInteractionDTO) throws URISyntaxException {
        log.debug("REST request to save UserConversationInteraction : {}", userConversationInteractionDTO);
        if (userConversationInteractionDTO.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new userConversationInteraction cannot already have an ID").body(null);
        }
        UserConversationInteraction userConversationInteraction = userConversationInteractionMapper.userConversationInteractionDTOToUserConversationInteraction(userConversationInteractionDTO);
        UserConversationInteraction result = userConversationInteractionService.createUserConversationInteraction(userConversationInteraction);
        return ResponseEntity.created(new URI("/api/userConversationInteractions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("userConversationInteraction", result.getId().toString()))
            .body(userConversationInteractionMapper.userConversationInteractionToUserConversationInteractionDTO(result));
    }

    /**
     * PUT  /userConversationInteractions -> Updates an existing userConversationInteraction.
     */
    @RequestMapping(value = "/userConversationInteractions",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<UserConversationInteractionDTO> updateUserConversationInteraction(@RequestBody UserConversationInteractionDTO userConversationInteractionDTO) throws URISyntaxException {
        log.debug("REST request to update UserConversationInteraction : {}", userConversationInteractionDTO);
        if (userConversationInteractionDTO.getId() == null) {
            return createUserConversationInteraction(userConversationInteractionDTO);
        }
        UserConversationInteraction userConversationInteraction = userConversationInteractionMapper.userConversationInteractionDTOToUserConversationInteraction(userConversationInteractionDTO);
        UserConversationInteraction result = userConversationInteractionService.updateUserConversationInteraction(userConversationInteraction);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("userConversationInteraction", userConversationInteractionDTO.getId().toString()))
            .body(userConversationInteractionMapper.userConversationInteractionToUserConversationInteractionDTO(result));
    }

    /**
     * GET  /userConversationInteractions -> get all the userConversationInteractions.
     */
    @RequestMapping(value = "/userConversationInteractions",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<UserConversationInteractionDTO>> getAllUserConversationInteractions(Pageable pageable)
        throws URISyntaxException {
        Page<UserConversationInteraction> page = userConversationInteractionService.getAllUserConversationInteractions(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/userConversationInteractions");
        return new ResponseEntity<>(page.getContent().stream()
            .map(userConversationInteractionMapper::userConversationInteractionToUserConversationInteractionDTO)
            .collect(Collectors.toCollection(LinkedList::new)), headers, HttpStatus.OK);
    }

    /**
     * GET  /userConversationInteractions/:id -> get the "id" userConversationInteraction.
     */
    @RequestMapping(value = "/userConversationInteractions/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<UserConversationInteractionDTO> getUserConversationInteraction(@PathVariable Long id) {
        log.debug("REST request to get UserConversationInteraction : {}", id);
        return Optional.ofNullable(userConversationInteractionService.getUserConversationInteraction(id))
            .map(userConversationInteractionMapper::userConversationInteractionToUserConversationInteractionDTO)
            .map(userConversationInteractionDTO -> new ResponseEntity<>(
                userConversationInteractionDTO,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /userConversationInteractions/:id -> delete the "id" userConversationInteraction.
     */
    @RequestMapping(value = "/userConversationInteractions/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteUserConversationInteraction(@PathVariable Long id) {
        log.debug("REST request to delete UserConversationInteraction : {}", id);
        userConversationInteractionService.deleteUserConversationInteraction(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("userConversationInteraction", id.toString())).build();
    }
}
