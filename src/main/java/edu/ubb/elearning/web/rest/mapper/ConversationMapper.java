package edu.ubb.elearning.web.rest.mapper;

import edu.ubb.elearning.domain.*;
import edu.ubb.elearning.web.rest.dto.ConversationDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Conversation and its DTO ConversationDTO.
 */
@Mapper(componentModel = "spring", uses = {ConversationCommentMapper.class, })
public interface ConversationMapper {

    @Mapping(source = "course.id", target = "courseId")
    @Mapping(source = "course.name", target = "courseName")
    @Mapping(source = "groups.id", target = "groupsId")
    @Mapping(source = "groups.name", target = "groupsName")
    ConversationDTO conversationToConversationDTO(Conversation conversation);

    @Mapping(source = "courseId", target = "course")
    @Mapping(source = "groupsId", target = "groups")
    Conversation conversationDTOToConversation(ConversationDTO conversationDTO);

    default Course courseFromId(Long id) {
        if (id == null) {
            return null;
        }
        Course course = new Course();
        course.setId(id);
        return course;
    }

    default Groups groupsFromId(Long id) {
        if (id == null) {
            return null;
        }
        Groups groups = new Groups();
        groups.setId(id);
        return groups;
    }

    default ConversationComment conversationCommentFromId(Long id) {
        if (id == null) {
            return null;
        }
        ConversationComment conversationComment = new ConversationComment();
        conversationComment.setId(id);
        return conversationComment;
    }
}
