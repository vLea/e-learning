package edu.ubb.elearning.web.rest;

import com.codahale.metrics.annotation.Timed;
import edu.ubb.elearning.domain.Pages;
import edu.ubb.elearning.service.PagesService;
import edu.ubb.elearning.web.rest.util.HeaderUtil;
import edu.ubb.elearning.web.rest.util.PaginationUtil;
import edu.ubb.elearning.web.rest.dto.PagesDTO;
import edu.ubb.elearning.web.rest.mapper.PagesMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * REST controller for managing Pages.
 */
@RestController
@RequestMapping("/api")
public class PagesResource {

    private final Logger log = LoggerFactory.getLogger(PagesResource.class);

    @Inject
    private PagesService pagesService;

    @Inject
    private PagesMapper pagesMapper;

    /**
     * POST  /pagess -> Create a new pages.
     */
    @RequestMapping(value = "/pagess",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PagesDTO> createPages(@RequestBody PagesDTO pagesDTO) throws URISyntaxException {
        log.debug("REST request to save Pages : {}", pagesDTO);
        if (pagesDTO.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new pages cannot already have an ID").body(null);
        }
        Pages pages = pagesMapper.pagesDTOToPages(pagesDTO);
        Pages result = pagesService.createPages(pages);
        return ResponseEntity.created(new URI("/api/pagess/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("pages", result.getId().toString()))
            .body(pagesMapper.pagesToPagesDTO(result));
    }

    /**
     * PUT  /pagess -> Updates an existing pages.
     */
    @RequestMapping(value = "/pagess",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PagesDTO> updatePages(@RequestBody PagesDTO pagesDTO) throws URISyntaxException {
        log.debug("REST request to update Pages : {}", pagesDTO);
        if (pagesDTO.getId() == null) {
            return createPages(pagesDTO);
        }
        Pages pages = pagesMapper.pagesDTOToPages(pagesDTO);
        Pages result = pagesService.updatePages(pages);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("pages", pagesDTO.getId().toString()))
            .body(pagesMapper.pagesToPagesDTO(result));
    }

    /**
     * GET  /pagess -> get all the pagess.
     */
    @RequestMapping(value = "/pagess",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<PagesDTO>> getAllPagess(Pageable pageable)
        throws URISyntaxException {
        Page<Pages> page = pagesService.getAllPagess(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/pagess");
        return new ResponseEntity<>(page.getContent().stream()
            .map(pagesMapper::pagesToPagesDTO)
            .collect(Collectors.toCollection(LinkedList::new)), headers, HttpStatus.OK);
    }

    /**
     * GET  /pagess/:id -> get the "id" pages.
     */
    @RequestMapping(value = "/pagess/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PagesDTO> getPages(@PathVariable Long id) {
        log.debug("REST request to get Pages : {}", id);
        return Optional.ofNullable(pagesService.getPages(id))
            .map(pagesMapper::pagesToPagesDTO)
            .map(pagesDTO -> new ResponseEntity<>(
                pagesDTO,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /pagess/:id -> delete the "id" pages.
     */
    @RequestMapping(value = "/pagess/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deletePages(@PathVariable Long id) {
        log.debug("REST request to delete Pages : {}", id);
        pagesService.deletePages(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("pages", id.toString())).build();
    }
}
