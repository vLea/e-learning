package edu.ubb.elearning.web.rest.mapper;

import edu.ubb.elearning.domain.*;
import edu.ubb.elearning.web.rest.dto.AssignmentDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Assignment and its DTO AssignmentDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface AssignmentMapper {

    @Mapping(source = "groups.id", target = "groupsId")
    @Mapping(source = "groups.name", target = "groupsName")
    @Mapping(source = "course.id", target = "courseId")
    @Mapping(source = "course.name", target = "courseName")
    AssignmentDTO assignmentToAssignmentDTO(Assignment assignment);

    @Mapping(source = "groupsId", target = "groups")
    @Mapping(source = "courseId", target = "course")
    Assignment assignmentDTOToAssignment(AssignmentDTO assignmentDTO);

    default Groups groupsFromId(Long id) {
        if (id == null) {
            return null;
        }
        Groups groups = new Groups();
        groups.setId(id);
        return groups;
    }

    default Course courseFromId(Long id) {
        if (id == null) {
            return null;
        }
        Course course = new Course();
        course.setId(id);
        return course;
    }
}
