package edu.ubb.elearning.web.rest.mapper;

import java.util.Set;

import org.mapstruct.Mapper;

import edu.ubb.elearning.domain.Authority;
import edu.ubb.elearning.web.rest.dto.AuthorityDTO;

@Mapper(componentModel = "spring", uses = { })
public interface AuthorityMapper {
	
	AuthorityDTO authorityToAuthorityDTO(Authority authority);

    Authority authorityDTOToAuthority(AuthorityDTO authorityDTO);
    
    //This needs a correct solution!!! - used for the GROUPSDTO
    Authority authorityToString(String value);

    //This needs a correct solution!!! - used for the GROUPSDTO
    String stringToAuthority(Authority value);
}
