package edu.ubb.elearning.repository;

import edu.ubb.elearning.domain.ExternalResource;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the ExternalResource entity.
 */
public interface ExternalResourceRepository extends JpaRepository<ExternalResource,Long> {

}
