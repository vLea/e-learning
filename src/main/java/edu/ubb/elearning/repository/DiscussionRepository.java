package edu.ubb.elearning.repository;

import edu.ubb.elearning.domain.Discussion;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Discussion entity.
 */
public interface DiscussionRepository extends JpaRepository<Discussion,Long> {

}
