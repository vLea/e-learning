package edu.ubb.elearning.repository;

import edu.ubb.elearning.domain.Groups;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the Groups entity.
 */
public interface GroupsRepository extends JpaRepository<Groups,Long> {

    @Query("select distinct groups from Groups groups left join fetch groups.users")
    List<Groups> findAllWithEagerRelationships();

    @Query("select groups from Groups groups left join fetch groups.users where groups.id =:id")
    Groups findOneWithEagerRelationships(@Param("id") Long id);

}
