package edu.ubb.elearning.repository;

import edu.ubb.elearning.domain.Pages;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the Pages entity.
 */
public interface PagesRepository extends JpaRepository<Pages,Long> {

    @Query("select distinct pages from Pages pages left join fetch pages.files")
    List<Pages> findAllWithEagerRelationships();

    @Query("select pages from Pages pages left join fetch pages.files where pages.id =:id")
    Pages findOneWithEagerRelationships(@Param("id") Long id);

}
