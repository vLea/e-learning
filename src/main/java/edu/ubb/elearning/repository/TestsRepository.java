package edu.ubb.elearning.repository;

import edu.ubb.elearning.domain.Tests;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Tests entity.
 */
public interface TestsRepository extends JpaRepository<Tests,Long> {

}
