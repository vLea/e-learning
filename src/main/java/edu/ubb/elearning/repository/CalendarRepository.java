package edu.ubb.elearning.repository;

import edu.ubb.elearning.domain.Calendar;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the Calendar entity.
 */
public interface CalendarRepository extends JpaRepository<Calendar,Long> {

    @Query("select calendar from Calendar calendar where calendar.user.login = ?#{principal.username}")
    List<Calendar> findByUserIsCurrentUser();

    @Query("select distinct calendar from Calendar calendar left join fetch calendar.events")
    List<Calendar> findAllWithEagerRelationships();

    @Query("select calendar from Calendar calendar left join fetch calendar.events where calendar.id =:id")
    Calendar findOneWithEagerRelationships(@Param("id") Long id);

}
