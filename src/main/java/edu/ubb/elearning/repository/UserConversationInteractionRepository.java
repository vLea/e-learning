package edu.ubb.elearning.repository;

import edu.ubb.elearning.domain.UserConversationInteraction;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the UserConversationInteraction entity.
 */
public interface UserConversationInteractionRepository extends JpaRepository<UserConversationInteraction,Long> {

    @Query("select userConversationInteraction from UserConversationInteraction userConversationInteraction where userConversationInteraction.user.login = ?#{principal.username}")
    List<UserConversationInteraction> findByUserIsCurrentUser();

}
