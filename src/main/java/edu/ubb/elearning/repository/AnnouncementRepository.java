package edu.ubb.elearning.repository;

import edu.ubb.elearning.domain.Announcement;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the Announcement entity.
 */
public interface AnnouncementRepository extends JpaRepository<Announcement,Long> {

    @Query("select distinct announcement from Announcement announcement left join fetch announcement.files")
    List<Announcement> findAllWithEagerRelationships();

    @Query("select announcement from Announcement announcement left join fetch announcement.files where announcement.id =:id")
    Announcement findOneWithEagerRelationships(@Param("id") Long id);

}
