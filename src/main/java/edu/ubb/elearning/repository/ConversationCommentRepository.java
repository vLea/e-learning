package edu.ubb.elearning.repository;

import edu.ubb.elearning.domain.ConversationComment;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the ConversationComment entity.
 */
public interface ConversationCommentRepository extends JpaRepository<ConversationComment,Long> {

    @Query("select conversationComment from ConversationComment conversationComment where conversationComment.user.login = ?#{principal.username}")
    List<ConversationComment> findByUserIsCurrentUser();

}
