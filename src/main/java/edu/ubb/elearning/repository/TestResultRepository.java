package edu.ubb.elearning.repository;

import edu.ubb.elearning.domain.TestResult;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the TestResult entity.
 */
public interface TestResultRepository extends JpaRepository<TestResult,Long> {

    @Query("select testResult from TestResult testResult where testResult.user.login = ?#{principal.username}")
    List<TestResult> findByUserIsCurrentUser();

}
