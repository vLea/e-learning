package edu.ubb.elearning.repository;

import edu.ubb.elearning.domain.Assignment;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Assignment entity.
 */
public interface AssignmentRepository extends JpaRepository<Assignment,Long> {

}
