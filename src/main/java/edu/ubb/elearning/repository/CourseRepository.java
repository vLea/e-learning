package edu.ubb.elearning.repository;

import edu.ubb.elearning.domain.Course;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the Course entity.
 */
public interface CourseRepository extends JpaRepository<Course,Long> {

    @Query("select distinct course from Course course left join fetch course.users")
    List<Course> findAllWithEagerRelationships();

    @Query("select course from Course course left join fetch course.users where course.id =:id")
    Course findOneWithEagerRelationships(@Param("id") Long id);

}
