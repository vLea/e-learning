package edu.ubb.elearning.repository;

import edu.ubb.elearning.domain.Message;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the Message entity.
 */
public interface MessageRepository extends JpaRepository<Message,Long> {

    @Query("select message from Message message where message.userFrom.login = ?#{principal.username}")
    List<Message> findByUserFromIsCurrentUser();

    @Query("select message from Message message where message.userTo.login = ?#{principal.username}")
    List<Message> findByUserToIsCurrentUser();

    @Query("select distinct message from Message message left join fetch message.files")
    List<Message> findAllWithEagerRelationships();

    @Query("select message from Message message left join fetch message.files where message.id =:id")
    Message findOneWithEagerRelationships(@Param("id") Long id);

}
