'use strict';

angular.module('elearningApp')
    .controller('MessageDetailController', function ($scope, $rootScope, $stateParams, entity, Message, Conversation, User, File) {
        $scope.message = entity;
        $scope.load = function (id) {
            Message.get({id: id}, function(result) {
                $scope.message = result;
            });
        };
        var unsubscribe = $rootScope.$on('elearningApp:messageUpdate', function(event, result) {
            $scope.message = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
