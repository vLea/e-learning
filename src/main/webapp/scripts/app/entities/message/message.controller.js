'use strict';

angular.module('elearningApp')
    .controller('MessageController', function ($scope, $state, $modal, Message, ParseLinks) {
      
        $scope.messages = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            Message.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.messages = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.message = {
                subject: null,
                text: null,
                id: null
            };
        };
    });
