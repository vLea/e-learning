'use strict';

angular.module('elearningApp').controller('MessageDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'Message', 'Conversation', 'User', 'File',
        function($scope, $stateParams, $modalInstance, entity, Message, Conversation, User, File) {

        $scope.message = entity;
        $scope.conversations = Conversation.query();
        $scope.users = User.query();
        $scope.files = File.query();
        $scope.load = function(id) {
            Message.get({id : id}, function(result) {
                $scope.message = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('elearningApp:messageUpdate', result);
            $modalInstance.close(result);
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if ($scope.message.id != null) {
                Message.update($scope.message, onSaveSuccess, onSaveError);
            } else {
                Message.save($scope.message, onSaveSuccess, onSaveError);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);
