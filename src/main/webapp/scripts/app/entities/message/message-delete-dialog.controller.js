'use strict';

angular.module('elearningApp')
	.controller('MessageDeleteController', function($scope, $modalInstance, entity, Message) {

        $scope.message = entity;
        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            Message.delete({id: id},
                function () {
                    $modalInstance.close(true);
                });
        };

    });