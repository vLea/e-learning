'use strict';

angular.module('elearningApp')
    .controller('TeacherDetailController', function ($scope, $rootScope, $stateParams, entity, Teacher, User) {
        $scope.teacher = entity;
        $scope.load = function (id) {
            Teacher.get({id: id}, function(result) {
                $scope.teacher = result;
            });
        };
        var unsubscribe = $rootScope.$on('elearningApp:teacherUpdate', function(event, result) {
            $scope.teacher = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
