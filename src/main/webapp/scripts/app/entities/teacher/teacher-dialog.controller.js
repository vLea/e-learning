'use strict';

angular.module('elearningApp').controller('TeacherDialogController',
    ['$scope', '$stateParams', '$modalInstance', '$q', 'entity', 'Teacher', 'User',
        function($scope, $stateParams, $modalInstance, $q, entity, Teacher, User) {

        $scope.teacher = entity;
        $scope.users = User.query();
        $scope.load = function(id) {
            Teacher.get({id : id}, function(result) {
                $scope.teacher = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('elearningApp:teacherUpdate', result);
            $modalInstance.close(result);
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if ($scope.teacher.id != null) {
                Teacher.update($scope.teacher, onSaveSuccess, onSaveError);
            } else {
                Teacher.save($scope.teacher, onSaveSuccess, onSaveError);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);
