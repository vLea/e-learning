'use strict';

angular.module('elearningApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('discussion', {
                parent: 'entity',
                url: '/discussions',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'Discussions'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/discussion/discussions.html',
                        controller: 'DiscussionController'
                    }
                },
                resolve: {
                }
            })
            .state('discussion.detail', {
                parent: 'entity',
                url: '/discussion/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'Discussion'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/discussion/discussion-detail.html',
                        controller: 'DiscussionDetailController'
                    }
                },
                resolve: {
                    entity: ['$stateParams', 'Discussion', function($stateParams, Discussion) {
                        return Discussion.get({id : $stateParams.id});
                    }]
                }
            })
            .state('discussion.new', {
                parent: 'discussion',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/discussion/discussion-dialog.html',
                        controller: 'DiscussionDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    name: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('discussion', null, { reload: true });
                    }, function() {
                        $state.go('discussion');
                    })
                }]
            })
            .state('discussion.edit', {
                parent: 'discussion',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/discussion/discussion-dialog.html',
                        controller: 'DiscussionDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['Discussion', function(Discussion) {
                                return Discussion.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('discussion', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            })
            .state('discussion.delete', {
                parent: 'discussion',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/discussion/discussion-delete-dialog.html',
                        controller: 'DiscussionDeleteController',
                        size: 'md',
                        resolve: {
                            entity: ['Discussion', function(Discussion) {
                                return Discussion.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('discussion', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
