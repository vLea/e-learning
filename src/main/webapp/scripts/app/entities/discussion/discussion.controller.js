'use strict';

angular.module('elearningApp')
    .controller('DiscussionController', function ($scope, $state, $modal, Discussion, ParseLinks) {
      
        $scope.discussions = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            Discussion.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.discussions = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.discussion = {
                name: null,
                id: null
            };
        };
    });
