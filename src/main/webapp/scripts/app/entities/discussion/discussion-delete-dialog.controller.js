'use strict';

angular.module('elearningApp')
	.controller('DiscussionDeleteController', function($scope, $modalInstance, entity, Discussion) {

        $scope.discussion = entity;
        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            Discussion.delete({id: id},
                function () {
                    $modalInstance.close(true);
                });
        };

    });