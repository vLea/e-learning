'use strict';

angular.module('elearningApp')
    .controller('DiscussionDetailController', function ($scope, $rootScope, $stateParams, entity, Discussion, Groups) {
        $scope.discussion = entity;
        $scope.load = function (id) {
            Discussion.get({id: id}, function(result) {
                $scope.discussion = result;
            });
        };
        var unsubscribe = $rootScope.$on('elearningApp:discussionUpdate', function(event, result) {
            $scope.discussion = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
