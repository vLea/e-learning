'use strict';

angular.module('elearningApp').controller('DiscussionDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'Discussion', 'Groups',
        function($scope, $stateParams, $modalInstance, entity, Discussion, Groups) {

        $scope.discussion = entity;
        $scope.groupss = Groups.query();
        $scope.load = function(id) {
            Discussion.get({id : id}, function(result) {
                $scope.discussion = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('elearningApp:discussionUpdate', result);
            $modalInstance.close(result);
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if ($scope.discussion.id != null) {
                Discussion.update($scope.discussion, onSaveSuccess, onSaveError);
            } else {
                Discussion.save($scope.discussion, onSaveSuccess, onSaveError);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);
