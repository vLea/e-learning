'use strict';

angular.module('elearningApp')
    .controller('AnnouncementDetailController', function ($scope, $rootScope, $stateParams, entity, Announcement, File, Groups) {
        $scope.announcement = entity;
        $scope.load = function (id) {
            Announcement.get({id: id}, function(result) {
                $scope.announcement = result;
            });
        };
        var unsubscribe = $rootScope.$on('elearningApp:announcementUpdate', function(event, result) {
            $scope.announcement = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
