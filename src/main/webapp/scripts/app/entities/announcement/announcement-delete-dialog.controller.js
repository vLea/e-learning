'use strict';

angular.module('elearningApp')
	.controller('AnnouncementDeleteController', function($scope, $modalInstance, entity, Announcement) {

        $scope.announcement = entity;
        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            Announcement.delete({id: id},
                function () {
                    $modalInstance.close(true);
                });
        };

    });