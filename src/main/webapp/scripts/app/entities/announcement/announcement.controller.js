'use strict';

angular.module('elearningApp')
    .controller('AnnouncementController', function ($scope, $state, $modal, Announcement, ParseLinks) {
      
        $scope.announcements = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            Announcement.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.announcements = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.announcement = {
                title: null,
                comment: null,
                id: null
            };
        };
    });
