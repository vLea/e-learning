'use strict';

angular.module('elearningApp').controller('AnnouncementDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'Announcement', 'File', 'Groups',
        function($scope, $stateParams, $modalInstance, entity, Announcement, File, Groups) {

        $scope.announcement = entity;
        $scope.files = File.query();
        $scope.groupss = Groups.query();
        $scope.load = function(id) {
            Announcement.get({id : id}, function(result) {
                $scope.announcement = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('elearningApp:announcementUpdate', result);
            $modalInstance.close(result);
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if ($scope.announcement.id != null) {
                Announcement.update($scope.announcement, onSaveSuccess, onSaveError);
            } else {
                Announcement.save($scope.announcement, onSaveSuccess, onSaveError);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);
