'use strict';

angular.module('elearningApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('userConversationInteraction', {
                parent: 'entity',
                url: '/userConversationInteractions',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'UserConversationInteractions'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/userConversationInteraction/userConversationInteractions.html',
                        controller: 'UserConversationInteractionController'
                    }
                },
                resolve: {
                }
            })
            .state('userConversationInteraction.detail', {
                parent: 'entity',
                url: '/userConversationInteraction/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'UserConversationInteraction'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/userConversationInteraction/userConversationInteraction-detail.html',
                        controller: 'UserConversationInteractionDetailController'
                    }
                },
                resolve: {
                    entity: ['$stateParams', 'UserConversationInteraction', function($stateParams, UserConversationInteraction) {
                        return UserConversationInteraction.get({id : $stateParams.id});
                    }]
                }
            })
            .state('userConversationInteraction.new', {
                parent: 'userConversationInteraction',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/userConversationInteraction/userConversationInteraction-dialog.html',
                        controller: 'UserConversationInteractionDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    archived: null,
                                    favorited: null,
                                    isRead: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('userConversationInteraction', null, { reload: true });
                    }, function() {
                        $state.go('userConversationInteraction');
                    })
                }]
            })
            .state('userConversationInteraction.edit', {
                parent: 'userConversationInteraction',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/userConversationInteraction/userConversationInteraction-dialog.html',
                        controller: 'UserConversationInteractionDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['UserConversationInteraction', function(UserConversationInteraction) {
                                return UserConversationInteraction.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('userConversationInteraction', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            })
            .state('userConversationInteraction.delete', {
                parent: 'userConversationInteraction',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/userConversationInteraction/userConversationInteraction-delete-dialog.html',
                        controller: 'UserConversationInteractionDeleteController',
                        size: 'md',
                        resolve: {
                            entity: ['UserConversationInteraction', function(UserConversationInteraction) {
                                return UserConversationInteraction.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('userConversationInteraction', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
