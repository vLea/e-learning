'use strict';

angular.module('elearningApp')
	.controller('UserConversationInteractionDeleteController', function($scope, $modalInstance, entity, UserConversationInteraction) {

        $scope.userConversationInteraction = entity;
        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            UserConversationInteraction.delete({id: id},
                function () {
                    $modalInstance.close(true);
                });
        };

    });