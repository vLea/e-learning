'use strict';

angular.module('elearningApp')
    .controller('UserConversationInteractionController', function ($scope, $state, $modal, UserConversationInteraction, ParseLinks) {
      
        $scope.userConversationInteractions = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            UserConversationInteraction.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.userConversationInteractions = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.userConversationInteraction = {
                archived: null,
                favorited: null,
                isRead: null,
                id: null
            };
        };
    });
