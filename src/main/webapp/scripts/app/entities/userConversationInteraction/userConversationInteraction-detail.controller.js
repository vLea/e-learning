'use strict';

angular.module('elearningApp')
    .controller('UserConversationInteractionDetailController', function ($scope, $rootScope, $stateParams, entity, UserConversationInteraction, User, Conversation) {
        $scope.userConversationInteraction = entity;
        $scope.load = function (id) {
            UserConversationInteraction.get({id: id}, function(result) {
                $scope.userConversationInteraction = result;
            });
        };
        var unsubscribe = $rootScope.$on('elearningApp:userConversationInteractionUpdate', function(event, result) {
            $scope.userConversationInteraction = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
