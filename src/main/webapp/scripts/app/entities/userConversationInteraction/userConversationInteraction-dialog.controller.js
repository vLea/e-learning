'use strict';

angular.module('elearningApp').controller('UserConversationInteractionDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'UserConversationInteraction', 'User', 'Conversation',
        function($scope, $stateParams, $modalInstance, entity, UserConversationInteraction, User, Conversation) {

        $scope.userConversationInteraction = entity;
        $scope.users = User.query();
        $scope.conversations = Conversation.query();
        $scope.load = function(id) {
            UserConversationInteraction.get({id : id}, function(result) {
                $scope.userConversationInteraction = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('elearningApp:userConversationInteractionUpdate', result);
            $modalInstance.close(result);
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if ($scope.userConversationInteraction.id != null) {
                UserConversationInteraction.update($scope.userConversationInteraction, onSaveSuccess, onSaveError);
            } else {
                UserConversationInteraction.save($scope.userConversationInteraction, onSaveSuccess, onSaveError);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);
