'use strict';

angular.module('elearningApp')
    .controller('FileController', function ($scope, $state, $modal, File, ParseLinks) {
      
        $scope.files = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            File.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.files = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.file = {
                name: null,
                parent: null,
                id: null
            };
        };
    });
