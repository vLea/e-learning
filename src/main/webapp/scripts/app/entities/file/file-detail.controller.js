'use strict';

angular.module('elearningApp')
    .controller('FileDetailController', function ($scope, $rootScope, $stateParams, entity, File) {
        $scope.file = entity;
        $scope.load = function (id) {
            File.get({id: id}, function(result) {
                $scope.file = result;
            });
        };
        var unsubscribe = $rootScope.$on('elearningApp:fileUpdate', function(event, result) {
            $scope.file = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
