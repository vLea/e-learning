'use strict';

angular.module('elearningApp')
	.controller('ConversationCommentDeleteController', function($scope, $modalInstance, entity, ConversationComment) {

        $scope.conversationComment = entity;
        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            ConversationComment.delete({id: id},
                function () {
                    $modalInstance.close(true);
                });
        };

    });