'use strict';

angular.module('elearningApp')
    .controller('ConversationCommentController', function ($scope, $state, $modal, ConversationComment, ParseLinks) {
      
        $scope.conversationComments = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            ConversationComment.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.conversationComments = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.conversationComment = {
                title: null,
                text: null,
                id: null
            };
        };
    });
