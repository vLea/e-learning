'use strict';

angular.module('elearningApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('conversationComment', {
                parent: 'entity',
                url: '/conversationComments',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'ConversationComments'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/conversationComment/conversationComments.html',
                        controller: 'ConversationCommentController'
                    }
                },
                resolve: {
                }
            })
            .state('conversationComment.detail', {
                parent: 'entity',
                url: '/conversationComment/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'ConversationComment'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/conversationComment/conversationComment-detail.html',
                        controller: 'ConversationCommentDetailController'
                    }
                },
                resolve: {
                    entity: ['$stateParams', 'ConversationComment', function($stateParams, ConversationComment) {
                        return ConversationComment.get({id : $stateParams.id});
                    }]
                }
            })
            .state('conversationComment.new', {
                parent: 'conversationComment',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/conversationComment/conversationComment-dialog.html',
                        controller: 'ConversationCommentDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    title: null,
                                    text: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('conversationComment', null, { reload: true });
                    }, function() {
                        $state.go('conversationComment');
                    })
                }]
            })
            .state('conversationComment.edit', {
                parent: 'conversationComment',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/conversationComment/conversationComment-dialog.html',
                        controller: 'ConversationCommentDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['ConversationComment', function(ConversationComment) {
                                return ConversationComment.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('conversationComment', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            })
            .state('conversationComment.delete', {
                parent: 'conversationComment',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/conversationComment/conversationComment-delete-dialog.html',
                        controller: 'ConversationCommentDeleteController',
                        size: 'md',
                        resolve: {
                            entity: ['ConversationComment', function(ConversationComment) {
                                return ConversationComment.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('conversationComment', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
