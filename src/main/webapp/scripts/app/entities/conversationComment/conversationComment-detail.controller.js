'use strict';

angular.module('elearningApp')
    .controller('ConversationCommentDetailController', function ($scope, $rootScope, $stateParams, entity, ConversationComment, User) {
        $scope.conversationComment = entity;
        $scope.load = function (id) {
            ConversationComment.get({id: id}, function(result) {
                $scope.conversationComment = result;
            });
        };
        var unsubscribe = $rootScope.$on('elearningApp:conversationCommentUpdate', function(event, result) {
            $scope.conversationComment = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
