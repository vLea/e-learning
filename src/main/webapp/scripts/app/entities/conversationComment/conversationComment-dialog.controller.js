'use strict';

angular.module('elearningApp').controller('ConversationCommentDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'ConversationComment', 'User',
        function($scope, $stateParams, $modalInstance, entity, ConversationComment, User) {

        $scope.conversationComment = entity;
        $scope.users = User.query();
        $scope.load = function(id) {
            ConversationComment.get({id : id}, function(result) {
                $scope.conversationComment = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('elearningApp:conversationCommentUpdate', result);
            $modalInstance.close(result);
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if ($scope.conversationComment.id != null) {
                ConversationComment.update($scope.conversationComment, onSaveSuccess, onSaveError);
            } else {
                ConversationComment.save($scope.conversationComment, onSaveSuccess, onSaveError);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);
