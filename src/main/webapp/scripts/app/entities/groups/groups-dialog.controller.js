'use strict';

angular.module('elearningApp').controller('GroupsDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'Groups', 'User',
        function($scope, $stateParams, $modalInstance, entity, Groups, User) {

        $scope.groups = entity;
        $scope.users = User.query();
        $scope.load = function(id) {
            Groups.get({id : id}, function(result) {
                $scope.groups = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('elearningApp:groupsUpdate', result);
            $modalInstance.close(result);
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if ($scope.groups.id != null) {
                Groups.update($scope.groups, onSaveSuccess, onSaveError);
            } else {
                Groups.save($scope.groups, onSaveSuccess, onSaveError);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);
