'use strict';

angular.module('elearningApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('groups', {
                parent: 'entity',
                url: '/groupss',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'Groupss'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/groups/groupss.html',
                        controller: 'GroupsController'
                    }
                },
                resolve: {
                }
            })
            .state('groups.detail', {
                parent: 'entity',
                url: '/groups/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'Groups'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/groups/groups-detail.html',
                        controller: 'GroupsDetailController'
                    }
                },
                resolve: {
                    entity: ['$stateParams', 'Groups', function($stateParams, Groups) {
                        return Groups.get({id : $stateParams.id});
                    }]
                }
            })
            .state('groups.new', {
                parent: 'groups',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/groups/groups-dialog.html',
                        controller: 'GroupsDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    name: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('groups', null, { reload: true });
                    }, function() {
                        $state.go('groups');
                    })
                }]
            })
            .state('groups.edit', {
                parent: 'groups',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/groups/groups-dialog.html',
                        controller: 'GroupsDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['Groups', function(Groups) {
                                return Groups.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('groups', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            })
            .state('groups.delete', {
                parent: 'groups',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/groups/groups-delete-dialog.html',
                        controller: 'GroupsDeleteController',
                        size: 'md',
                        resolve: {
                            entity: ['Groups', function(Groups) {
                                return Groups.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('groups', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
