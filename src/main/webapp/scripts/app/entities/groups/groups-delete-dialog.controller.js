'use strict';

angular.module('elearningApp')
	.controller('GroupsDeleteController', function($scope, $modalInstance, entity, Groups) {

        $scope.groups = entity;
        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            Groups.delete({id: id},
                function () {
                    $modalInstance.close(true);
                });
        };

    });