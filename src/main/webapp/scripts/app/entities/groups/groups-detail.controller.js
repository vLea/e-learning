'use strict';

angular.module('elearningApp')
    .controller('GroupsDetailController', function ($scope, $rootScope, $stateParams, entity, Groups, User) {
        $scope.groups = entity;
        $scope.load = function (id) {
            Groups.get({id: id}, function(result) {
                $scope.groups = result;
            });
        };
        var unsubscribe = $rootScope.$on('elearningApp:groupsUpdate', function(event, result) {
            $scope.groups = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
