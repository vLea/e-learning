'use strict';

angular.module('elearningApp')
    .controller('GroupsController', function ($scope, $state, $modal, Groups, ParseLinks) {
      
        $scope.groupss = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            Groups.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.groupss = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.groups = {
                name: null,
                id: null
            };
        };
    });
