'use strict';

angular.module('elearningApp')
    .controller('TestResultDetailController', function ($scope, $rootScope, $stateParams, entity, TestResult, User, Tests) {
        $scope.testResult = entity;
        $scope.load = function (id) {
            TestResult.get({id: id}, function(result) {
                $scope.testResult = result;
            });
        };
        var unsubscribe = $rootScope.$on('elearningApp:testResultUpdate', function(event, result) {
            $scope.testResult = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
