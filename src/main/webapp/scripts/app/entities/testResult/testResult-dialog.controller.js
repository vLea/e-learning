'use strict';

angular.module('elearningApp').controller('TestResultDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'TestResult', 'User', 'Tests',
        function($scope, $stateParams, $modalInstance, entity, TestResult, User, Tests) {

        $scope.testResult = entity;
        $scope.users = User.query();
        $scope.testss = Tests.query();
        $scope.load = function(id) {
            TestResult.get({id : id}, function(result) {
                $scope.testResult = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('elearningApp:testResultUpdate', result);
            $modalInstance.close(result);
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if ($scope.testResult.id != null) {
                TestResult.update($scope.testResult, onSaveSuccess, onSaveError);
            } else {
                TestResult.save($scope.testResult, onSaveSuccess, onSaveError);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);
