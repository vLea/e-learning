'use strict';

angular.module('elearningApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('testResult', {
                parent: 'entity',
                url: '/testResults',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'TestResults'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/testResult/testResults.html',
                        controller: 'TestResultController'
                    }
                },
                resolve: {
                }
            })
            .state('testResult.detail', {
                parent: 'entity',
                url: '/testResult/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'TestResult'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/testResult/testResult-detail.html',
                        controller: 'TestResultDetailController'
                    }
                },
                resolve: {
                    entity: ['$stateParams', 'TestResult', function($stateParams, TestResult) {
                        return TestResult.get({id : $stateParams.id});
                    }]
                }
            })
            .state('testResult.new', {
                parent: 'testResult',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/testResult/testResult-dialog.html',
                        controller: 'TestResultDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    mark: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('testResult', null, { reload: true });
                    }, function() {
                        $state.go('testResult');
                    })
                }]
            })
            .state('testResult.edit', {
                parent: 'testResult',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/testResult/testResult-dialog.html',
                        controller: 'TestResultDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['TestResult', function(TestResult) {
                                return TestResult.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('testResult', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            })
            .state('testResult.delete', {
                parent: 'testResult',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/testResult/testResult-delete-dialog.html',
                        controller: 'TestResultDeleteController',
                        size: 'md',
                        resolve: {
                            entity: ['TestResult', function(TestResult) {
                                return TestResult.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('testResult', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
