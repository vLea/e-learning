'use strict';

angular.module('elearningApp')
	.controller('TestResultDeleteController', function($scope, $modalInstance, entity, TestResult) {

        $scope.testResult = entity;
        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            TestResult.delete({id: id},
                function () {
                    $modalInstance.close(true);
                });
        };

    });