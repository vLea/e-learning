'use strict';

angular.module('elearningApp')
    .controller('TestResultController', function ($scope, $state, $modal, TestResult, ParseLinks) {
      
        $scope.testResults = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            TestResult.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.testResults = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.testResult = {
                mark: null,
                id: null
            };
        };
    });
