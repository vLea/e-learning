'use strict';

angular.module('elearningApp')
    .controller('AssignmentController', function ($scope, $state, $modal, Assignment, ParseLinks) {
      
        $scope.assignments = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            Assignment.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.assignments = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.assignment = {
                name: null,
                description: null,
                deadline: null,
                points: null,
                id: null
            };
        };
    });
