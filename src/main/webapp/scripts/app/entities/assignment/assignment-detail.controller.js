'use strict';

angular.module('elearningApp')
    .controller('AssignmentDetailController', function ($scope, $rootScope, $stateParams, entity, Assignment, Groups, Course) {
        $scope.assignment = entity;
        $scope.load = function (id) {
            Assignment.get({id: id}, function(result) {
                $scope.assignment = result;
            });
        };
        var unsubscribe = $rootScope.$on('elearningApp:assignmentUpdate', function(event, result) {
            $scope.assignment = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
