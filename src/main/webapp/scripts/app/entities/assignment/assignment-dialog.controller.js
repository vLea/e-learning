'use strict';

angular.module('elearningApp').controller('AssignmentDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'Assignment', 'Groups', 'Course',
        function($scope, $stateParams, $modalInstance, entity, Assignment, Groups, Course) {

        $scope.assignment = entity;
        $scope.groupss = Groups.query();
        $scope.courses = Course.query();
        $scope.load = function(id) {
            Assignment.get({id : id}, function(result) {
                $scope.assignment = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('elearningApp:assignmentUpdate', result);
            $modalInstance.close(result);
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if ($scope.assignment.id != null) {
                Assignment.update($scope.assignment, onSaveSuccess, onSaveError);
            } else {
                Assignment.save($scope.assignment, onSaveSuccess, onSaveError);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);
