'use strict';

angular.module('elearningApp')
	.controller('AssignmentDeleteController', function($scope, $modalInstance, entity, Assignment) {

        $scope.assignment = entity;
        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            Assignment.delete({id: id},
                function () {
                    $modalInstance.close(true);
                });
        };

    });