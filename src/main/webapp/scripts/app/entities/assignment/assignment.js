'use strict';

angular.module('elearningApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('assignment', {
                parent: 'entity',
                url: '/assignments',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'Assignments'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/assignment/assignments.html',
                        controller: 'AssignmentController'
                    }
                },
                resolve: {
                }
            })
            .state('assignment.detail', {
                parent: 'entity',
                url: '/assignment/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'Assignment'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/assignment/assignment-detail.html',
                        controller: 'AssignmentDetailController'
                    }
                },
                resolve: {
                    entity: ['$stateParams', 'Assignment', function($stateParams, Assignment) {
                        return Assignment.get({id : $stateParams.id});
                    }]
                }
            })
            .state('assignment.new', {
                parent: 'assignment',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/assignment/assignment-dialog.html',
                        controller: 'AssignmentDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    name: null,
                                    description: null,
                                    deadline: null,
                                    points: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('assignment', null, { reload: true });
                    }, function() {
                        $state.go('assignment');
                    })
                }]
            })
            .state('assignment.edit', {
                parent: 'assignment',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/assignment/assignment-dialog.html',
                        controller: 'AssignmentDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['Assignment', function(Assignment) {
                                return Assignment.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('assignment', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            })
            .state('assignment.delete', {
                parent: 'assignment',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/assignment/assignment-delete-dialog.html',
                        controller: 'AssignmentDeleteController',
                        size: 'md',
                        resolve: {
                            entity: ['Assignment', function(Assignment) {
                                return Assignment.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('assignment', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
