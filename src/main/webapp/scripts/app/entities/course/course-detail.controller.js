'use strict';

angular.module('elearningApp')
    .controller('CourseDetailController', function ($scope, $rootScope, $stateParams, entity, Course, Teacher, User) {
        $scope.course = entity;
        $scope.load = function (id) {
            Course.get({id: id}, function(result) {
                $scope.course = result;
            });
        };
        var unsubscribe = $rootScope.$on('elearningApp:courseUpdate', function(event, result) {
            $scope.course = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
