'use strict';

angular.module('elearningApp').controller('QuestionDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'Question', 'Tests',
        function($scope, $stateParams, $modalInstance, entity, Question, Tests) {

        $scope.question = entity;
        $scope.testss = Tests.query();
        $scope.load = function(id) {
            Question.get({id : id}, function(result) {
                $scope.question = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('elearningApp:questionUpdate', result);
            $modalInstance.close(result);
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if ($scope.question.id != null) {
                Question.update($scope.question, onSaveSuccess, onSaveError);
            } else {
                Question.save($scope.question, onSaveSuccess, onSaveError);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);
