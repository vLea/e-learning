'use strict';

angular.module('elearningApp')
    .controller('QuestionDetailController', function ($scope, $rootScope, $stateParams, entity, Question, Tests) {
        $scope.question = entity;
        $scope.load = function (id) {
            Question.get({id: id}, function(result) {
                $scope.question = result;
            });
        };
        var unsubscribe = $rootScope.$on('elearningApp:questionUpdate', function(event, result) {
            $scope.question = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
