'use strict';

angular.module('elearningApp')
    .controller('QuestionController', function ($scope, $state, $modal, Question, ParseLinks) {
      
        $scope.questions = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            Question.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.questions = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.question = {
                name: null,
                points: null,
                description: null,
                type: null,
                id: null
            };
        };
    });
