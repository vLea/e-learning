'use strict';

angular.module('elearningApp')
	.controller('QuestionDeleteController', function($scope, $modalInstance, entity, Question) {

        $scope.question = entity;
        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            Question.delete({id: id},
                function () {
                    $modalInstance.close(true);
                });
        };

    });