'use strict';

angular.module('elearningApp')
    .controller('CalendarController', function ($scope, $state, $modal, Calendar, ParseLinks) {
      
        $scope.calendars = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            Calendar.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.calendars = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.calendar = {
                id: null
            };
        };
    });
