'use strict';

angular.module('elearningApp')
    .controller('CalendarDetailController', function ($scope, $rootScope, $stateParams, entity, Calendar, User, Event) {
        $scope.calendar = entity;
        $scope.load = function (id) {
            Calendar.get({id: id}, function(result) {
                $scope.calendar = result;
            });
        };
        var unsubscribe = $rootScope.$on('elearningApp:calendarUpdate', function(event, result) {
            $scope.calendar = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
