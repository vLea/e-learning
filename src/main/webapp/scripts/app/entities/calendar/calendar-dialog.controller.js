'use strict';

angular.module('elearningApp').controller('CalendarDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'Calendar', 'User', 'Event',
        function($scope, $stateParams, $modalInstance, entity, Calendar, User, Event) {

        $scope.calendar = entity;
        $scope.users = User.query();
        $scope.events = Event.query();
        $scope.load = function(id) {
            Calendar.get({id : id}, function(result) {
                $scope.calendar = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('elearningApp:calendarUpdate', result);
            $modalInstance.close(result);
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if ($scope.calendar.id != null) {
                Calendar.update($scope.calendar, onSaveSuccess, onSaveError);
            } else {
                Calendar.save($scope.calendar, onSaveSuccess, onSaveError);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);
