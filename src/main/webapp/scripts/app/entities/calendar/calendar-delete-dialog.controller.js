'use strict';

angular.module('elearningApp')
	.controller('CalendarDeleteController', function($scope, $modalInstance, entity, Calendar) {

        $scope.calendar = entity;
        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            Calendar.delete({id: id},
                function () {
                    $modalInstance.close(true);
                });
        };

    });