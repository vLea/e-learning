'use strict';

angular.module('elearningApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('calendar', {
                parent: 'entity',
                url: '/calendars',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'Calendars'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/calendar/calendars.html',
                        controller: 'CalendarController'
                    }
                },
                resolve: {
                }
            })
            .state('calendar.detail', {
                parent: 'entity',
                url: '/calendar/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'Calendar'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/calendar/calendar-detail.html',
                        controller: 'CalendarDetailController'
                    }
                },
                resolve: {
                    entity: ['$stateParams', 'Calendar', function($stateParams, Calendar) {
                        return Calendar.get({id : $stateParams.id});
                    }]
                }
            })
            .state('calendar.new', {
                parent: 'calendar',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/calendar/calendar-dialog.html',
                        controller: 'CalendarDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    id: null
                                };
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('calendar', null, { reload: true });
                    }, function() {
                        $state.go('calendar');
                    })
                }]
            })
            .state('calendar.edit', {
                parent: 'calendar',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/calendar/calendar-dialog.html',
                        controller: 'CalendarDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['Calendar', function(Calendar) {
                                return Calendar.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('calendar', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            })
            .state('calendar.delete', {
                parent: 'calendar',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/calendar/calendar-delete-dialog.html',
                        controller: 'CalendarDeleteController',
                        size: 'md',
                        resolve: {
                            entity: ['Calendar', function(Calendar) {
                                return Calendar.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('calendar', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
