'use strict';

angular.module('elearningApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('externalResource', {
                parent: 'entity',
                url: '/externalResources',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'ExternalResources'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/externalResource/externalResources.html',
                        controller: 'ExternalResourceController'
                    }
                },
                resolve: {
                }
            })
            .state('externalResource.detail', {
                parent: 'entity',
                url: '/externalResource/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'ExternalResource'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/externalResource/externalResource-detail.html',
                        controller: 'ExternalResourceDetailController'
                    }
                },
                resolve: {
                    entity: ['$stateParams', 'ExternalResource', function($stateParams, ExternalResource) {
                        return ExternalResource.get({id : $stateParams.id});
                    }]
                }
            })
            .state('externalResource.new', {
                parent: 'externalResource',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/externalResource/externalResource-dialog.html',
                        controller: 'ExternalResourceDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    name: null,
                                    type: null,
                                    data: null,
                                    dataContentType: null,
                                    path: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('externalResource', null, { reload: true });
                    }, function() {
                        $state.go('externalResource');
                    })
                }]
            })
            .state('externalResource.edit', {
                parent: 'externalResource',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/externalResource/externalResource-dialog.html',
                        controller: 'ExternalResourceDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['ExternalResource', function(ExternalResource) {
                                return ExternalResource.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('externalResource', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            })
            .state('externalResource.delete', {
                parent: 'externalResource',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/externalResource/externalResource-delete-dialog.html',
                        controller: 'ExternalResourceDeleteController',
                        size: 'md',
                        resolve: {
                            entity: ['ExternalResource', function(ExternalResource) {
                                return ExternalResource.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('externalResource', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
