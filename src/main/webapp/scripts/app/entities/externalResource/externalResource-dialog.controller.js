'use strict';

angular.module('elearningApp').controller('ExternalResourceDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'DataUtils', 'entity', 'ExternalResource',
        function($scope, $stateParams, $modalInstance, DataUtils, entity, ExternalResource) {

        $scope.externalResource = entity;
        $scope.load = function(id) {
            ExternalResource.get({id : id}, function(result) {
                $scope.externalResource = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('elearningApp:externalResourceUpdate', result);
            $modalInstance.close(result);
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if ($scope.externalResource.id != null) {
                ExternalResource.update($scope.externalResource, onSaveSuccess, onSaveError);
            } else {
                ExternalResource.save($scope.externalResource, onSaveSuccess, onSaveError);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };

        $scope.abbreviate = DataUtils.abbreviate;

        $scope.byteSize = DataUtils.byteSize;

        $scope.setData = function ($file, externalResource) {
            if ($file) {
                var fileReader = new FileReader();
                fileReader.readAsDataURL($file);
                fileReader.onload = function (e) {
                    var base64Data = e.target.result.substr(e.target.result.indexOf('base64,') + 'base64,'.length);
                    $scope.$apply(function() {
                        externalResource.data = base64Data;
                        externalResource.dataContentType = $file.type;
                    });
                };
            }
        };
}]);
