'use strict';

angular.module('elearningApp')
	.controller('ExternalResourceDeleteController', function($scope, $modalInstance, entity, ExternalResource) {

        $scope.externalResource = entity;
        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            ExternalResource.delete({id: id},
                function () {
                    $modalInstance.close(true);
                });
        };

    });