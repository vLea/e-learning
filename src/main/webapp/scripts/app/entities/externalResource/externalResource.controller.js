'use strict';

angular.module('elearningApp')
    .controller('ExternalResourceController', function ($scope, $state, $modal, DataUtils, ExternalResource, ParseLinks) {
      
        $scope.externalResources = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            ExternalResource.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.externalResources = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.externalResource = {
                name: null,
                type: null,
                data: null,
                dataContentType: null,
                path: null,
                id: null
            };
        };

        $scope.abbreviate = DataUtils.abbreviate;

        $scope.byteSize = DataUtils.byteSize;
    });
