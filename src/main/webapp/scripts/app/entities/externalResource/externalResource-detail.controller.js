'use strict';

angular.module('elearningApp')
    .controller('ExternalResourceDetailController', function ($scope, $rootScope, $stateParams, DataUtils, entity, ExternalResource) {
        $scope.externalResource = entity;
        $scope.load = function (id) {
            ExternalResource.get({id: id}, function(result) {
                $scope.externalResource = result;
            });
        };
        var unsubscribe = $rootScope.$on('elearningApp:externalResourceUpdate', function(event, result) {
            $scope.externalResource = result;
        });
        $scope.$on('$destroy', unsubscribe);

        $scope.byteSize = DataUtils.byteSize;
    });
