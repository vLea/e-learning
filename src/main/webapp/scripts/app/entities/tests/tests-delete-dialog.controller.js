'use strict';

angular.module('elearningApp')
	.controller('TestsDeleteController', function($scope, $modalInstance, entity, Tests) {

        $scope.tests = entity;
        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            Tests.delete({id: id},
                function () {
                    $modalInstance.close(true);
                });
        };

    });