'use strict';

angular.module('elearningApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('tests', {
                parent: 'entity',
                url: '/testss',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'Testss'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/tests/testss.html',
                        controller: 'TestsController'
                    }
                },
                resolve: {
                }
            })
            .state('tests.detail', {
                parent: 'entity',
                url: '/tests/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'Tests'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/tests/tests-detail.html',
                        controller: 'TestsDetailController'
                    }
                },
                resolve: {
                    entity: ['$stateParams', 'Tests', function($stateParams, Tests) {
                        return Tests.get({id : $stateParams.id});
                    }]
                }
            })
            .state('tests.new', {
                parent: 'tests',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/tests/tests-dialog.html',
                        controller: 'TestsDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    name: null,
                                    description: null,
                                    deadline: null,
                                    type: null,
                                    startingDate: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('tests', null, { reload: true });
                    }, function() {
                        $state.go('tests');
                    })
                }]
            })
            .state('tests.edit', {
                parent: 'tests',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/tests/tests-dialog.html',
                        controller: 'TestsDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['Tests', function(Tests) {
                                return Tests.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('tests', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            })
            .state('tests.delete', {
                parent: 'tests',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/tests/tests-delete-dialog.html',
                        controller: 'TestsDeleteController',
                        size: 'md',
                        resolve: {
                            entity: ['Tests', function(Tests) {
                                return Tests.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('tests', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
