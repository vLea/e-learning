'use strict';

angular.module('elearningApp').controller('TestsDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'Tests', 'Course', 'Groups',
        function($scope, $stateParams, $modalInstance, entity, Tests, Course, Groups) {

        $scope.tests = entity;
        $scope.courses = Course.query();
        $scope.groupss = Groups.query();
        $scope.load = function(id) {
            Tests.get({id : id}, function(result) {
                $scope.tests = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('elearningApp:testsUpdate', result);
            $modalInstance.close(result);
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if ($scope.tests.id != null) {
                Tests.update($scope.tests, onSaveSuccess, onSaveError);
            } else {
                Tests.save($scope.tests, onSaveSuccess, onSaveError);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);
