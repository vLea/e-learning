'use strict';

angular.module('elearningApp')
    .controller('TestsController', function ($scope, $state, $modal, Tests, ParseLinks) {
      
        $scope.testss = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            Tests.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.testss = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.tests = {
                name: null,
                description: null,
                deadline: null,
                type: null,
                startingDate: null,
                id: null
            };
        };
    });
