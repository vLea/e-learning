'use strict';

angular.module('elearningApp')
    .controller('TestsDetailController', function ($scope, $rootScope, $stateParams, entity, Tests, Course, Groups) {
        $scope.tests = entity;
        $scope.load = function (id) {
            Tests.get({id: id}, function(result) {
                $scope.tests = result;
            });
        };
        var unsubscribe = $rootScope.$on('elearningApp:testsUpdate', function(event, result) {
            $scope.tests = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
