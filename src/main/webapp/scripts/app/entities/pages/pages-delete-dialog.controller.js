'use strict';

angular.module('elearningApp')
	.controller('PagesDeleteController', function($scope, $modalInstance, entity, Pages) {

        $scope.pages = entity;
        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            Pages.delete({id: id},
                function () {
                    $modalInstance.close(true);
                });
        };

    });