'use strict';

angular.module('elearningApp').controller('PagesDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'Pages', 'Groups', 'File',
        function($scope, $stateParams, $modalInstance, entity, Pages, Groups, File) {

        $scope.pages = entity;
        $scope.groupss = Groups.query();
        $scope.files = File.query();
        $scope.load = function(id) {
            Pages.get({id : id}, function(result) {
                $scope.pages = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('elearningApp:pagesUpdate', result);
            $modalInstance.close(result);
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if ($scope.pages.id != null) {
                Pages.update($scope.pages, onSaveSuccess, onSaveError);
            } else {
                Pages.save($scope.pages, onSaveSuccess, onSaveError);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);
