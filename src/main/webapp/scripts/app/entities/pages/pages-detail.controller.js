'use strict';

angular.module('elearningApp')
    .controller('PagesDetailController', function ($scope, $rootScope, $stateParams, entity, Pages, Groups, File) {
        $scope.pages = entity;
        $scope.load = function (id) {
            Pages.get({id: id}, function(result) {
                $scope.pages = result;
            });
        };
        var unsubscribe = $rootScope.$on('elearningApp:pagesUpdate', function(event, result) {
            $scope.pages = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
