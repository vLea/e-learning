'use strict';

angular.module('elearningApp')
    .controller('PagesController', function ($scope, $state, $modal, Pages, ParseLinks) {
      
        $scope.pagess = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            Pages.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.pagess = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.pages = {
                id: null
            };
        };
    });
