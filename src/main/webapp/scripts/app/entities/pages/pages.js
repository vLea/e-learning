'use strict';

angular.module('elearningApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('pages', {
                parent: 'entity',
                url: '/pagess',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'Pagess'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/pages/pagess.html',
                        controller: 'PagesController'
                    }
                },
                resolve: {
                }
            })
            .state('pages.detail', {
                parent: 'entity',
                url: '/pages/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'Pages'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/pages/pages-detail.html',
                        controller: 'PagesDetailController'
                    }
                },
                resolve: {
                    entity: ['$stateParams', 'Pages', function($stateParams, Pages) {
                        return Pages.get({id : $stateParams.id});
                    }]
                }
            })
            .state('pages.new', {
                parent: 'pages',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/pages/pages-dialog.html',
                        controller: 'PagesDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    id: null
                                };
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('pages', null, { reload: true });
                    }, function() {
                        $state.go('pages');
                    })
                }]
            })
            .state('pages.edit', {
                parent: 'pages',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/pages/pages-dialog.html',
                        controller: 'PagesDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['Pages', function(Pages) {
                                return Pages.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('pages', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            })
            .state('pages.delete', {
                parent: 'pages',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/pages/pages-delete-dialog.html',
                        controller: 'PagesDeleteController',
                        size: 'md',
                        resolve: {
                            entity: ['Pages', function(Pages) {
                                return Pages.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('pages', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
