'use strict';

angular.module('elearningApp')
    .controller('ConversationDetailController', function ($scope, $rootScope, $stateParams, entity, Conversation, Course, Groups, ConversationComment) {
        $scope.conversation = entity;
        $scope.load = function (id) {
            Conversation.get({id: id}, function(result) {
                $scope.conversation = result;
            });
        };
        var unsubscribe = $rootScope.$on('elearningApp:conversationUpdate', function(event, result) {
            $scope.conversation = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
