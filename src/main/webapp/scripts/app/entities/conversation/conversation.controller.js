'use strict';

angular.module('elearningApp')
    .controller('ConversationController', function ($scope, $state, $modal, Conversation, ParseLinks) {
      
        $scope.conversations = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            Conversation.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.conversations = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.conversation = {
                name: null,
                description: null,
                id: null
            };
        };
    });
