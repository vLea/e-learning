'use strict';

angular.module('elearningApp')
	.controller('ConversationDeleteController', function($scope, $modalInstance, entity, Conversation) {

        $scope.conversation = entity;
        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            Conversation.delete({id: id},
                function () {
                    $modalInstance.close(true);
                });
        };

    });