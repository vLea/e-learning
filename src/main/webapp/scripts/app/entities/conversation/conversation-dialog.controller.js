'use strict';

angular.module('elearningApp').controller('ConversationDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'Conversation', 'Course', 'Groups', 'ConversationComment',
        function($scope, $stateParams, $modalInstance, entity, Conversation, Course, Groups, ConversationComment) {

        $scope.conversation = entity;
        $scope.courses = Course.query();
        $scope.groupss = Groups.query();
        $scope.conversationcomments = ConversationComment.query();
        $scope.load = function(id) {
            Conversation.get({id : id}, function(result) {
                $scope.conversation = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('elearningApp:conversationUpdate', result);
            $modalInstance.close(result);
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if ($scope.conversation.id != null) {
                Conversation.update($scope.conversation, onSaveSuccess, onSaveError);
            } else {
                Conversation.save($scope.conversation, onSaveSuccess, onSaveError);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);
