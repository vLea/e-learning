'use strict';

angular.module('elearningApp')
    .factory('Assignment', function ($resource, DateUtils) {
        return $resource('api/assignments/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    data.deadline = DateUtils.convertLocaleDateFromServer(data.deadline);
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    data.deadline = DateUtils.convertLocaleDateToServer(data.deadline);
                    return angular.toJson(data);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    data.deadline = DateUtils.convertLocaleDateToServer(data.deadline);
                    return angular.toJson(data);
                }
            }
        });
    });
