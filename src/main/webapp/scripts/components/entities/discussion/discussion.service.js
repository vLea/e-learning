'use strict';

angular.module('elearningApp')
    .factory('Discussion', function ($resource, DateUtils) {
        return $resource('api/discussions/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    });
