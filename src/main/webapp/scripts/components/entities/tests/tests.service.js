'use strict';

angular.module('elearningApp')
    .factory('Tests', function ($resource, DateUtils) {
        return $resource('api/testss/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    data.deadline = DateUtils.convertLocaleDateFromServer(data.deadline);
                    data.startingDate = DateUtils.convertLocaleDateFromServer(data.startingDate);
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    data.deadline = DateUtils.convertLocaleDateToServer(data.deadline);
                    data.startingDate = DateUtils.convertLocaleDateToServer(data.startingDate);
                    return angular.toJson(data);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    data.deadline = DateUtils.convertLocaleDateToServer(data.deadline);
                    data.startingDate = DateUtils.convertLocaleDateToServer(data.startingDate);
                    return angular.toJson(data);
                }
            }
        });
    });
