'use strict';

angular.module('elearningApp')
    .factory('File', function ($resource, DateUtils) {
        return $resource('api/files/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    });
