'use strict';

angular.module('elearningApp')
    .factory('Groups', function ($resource, DateUtils) {
        return $resource('api/groupss/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    });
