'use strict';

angular.module('elearningApp')
    .factory('Pages', function ($resource, DateUtils) {
        return $resource('api/pagess/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    });
