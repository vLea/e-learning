'use strict';

angular.module('elearningApp')
    .factory('ExternalResource', function ($resource, DateUtils) {
        return $resource('api/externalResources/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    });
