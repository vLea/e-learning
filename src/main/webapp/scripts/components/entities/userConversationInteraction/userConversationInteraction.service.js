'use strict';

angular.module('elearningApp')
    .factory('UserConversationInteraction', function ($resource, DateUtils) {
        return $resource('api/userConversationInteractions/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    });
