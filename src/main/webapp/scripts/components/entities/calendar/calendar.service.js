'use strict';

angular.module('elearningApp')
    .factory('Calendar', function ($resource, DateUtils) {
        return $resource('api/calendars/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    });
