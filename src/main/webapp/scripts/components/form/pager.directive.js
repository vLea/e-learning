/* globals $ */
'use strict';

angular.module('elearningApp')
    .directive('elearningAppPager', function() {
        return {
            templateUrl: 'scripts/components/form/pager.html'
        };
    });
