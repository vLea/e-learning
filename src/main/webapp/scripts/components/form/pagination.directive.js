/* globals $ */
'use strict';

angular.module('elearningApp')
    .directive('elearningAppPagination', function() {
        return {
            templateUrl: 'scripts/components/form/pagination.html'
        };
    });
