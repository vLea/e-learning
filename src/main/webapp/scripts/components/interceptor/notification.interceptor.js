 'use strict';

angular.module('elearningApp')
    .factory('notificationInterceptor', function ($q, AlertService) {
        return {
            response: function(response) {
                var alertKey = response.headers('X-elearningApp-alert');
                if (angular.isString(alertKey)) {
                    AlertService.success(alertKey, { param : response.headers('X-elearningApp-params')});
                }
                return response;
            }
        };
    });
